/*
 * rp_pinout.h
 *
 *  Created on: Jun 4, 2014
 *      Author: raraujo
 */

#ifndef RP_PINOUT_H_
#define RP_PINOUT_H_

#define RP_SPI_CS_PORT		0
#define RP_SPI_CS_PIN		6

#define RP_SPI_CLK_PORT		0
#define RP_SPI_CLK_PIN		7

#define RP_SPI_MISO_PORT	0
#define RP_SPI_MISO_PIN		8

#define RP_SPI_MOSI_PORT	0
#define RP_SPI_MOSI_PIN		9

#define INTERRUPT_PORT		0
#define INTERRUPT_PIN		16

#define WLAN_RESET_PORT		0
#define WLAN_RESET_PIN		17

#endif /* RP_PINOUT_H_ */
