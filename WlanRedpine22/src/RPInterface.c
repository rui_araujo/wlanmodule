// *****************************************************************************
// ** Interface to RedPine WLAN Module **
// *****************************************************************************
#include "rp_pinout.h"
#include "RPinterface.h"
#include "rp_spi_driver.h"
#include "utils.h"
#include "RPLib.h"
#include "UART.h"

#include "chip.h"
#include <string.h>

// *****************************************************************************
#define TCP_PORT_OFFSET						((uint16_t) (+0))
//#define UDP_PORT_OFFSET						((uint16_t) (+2))
#define UDP_PORT_OFFSET						((uint16_t) (myCurrentIPAddress&0xFF))	// use individual UDP address for each active module!#define TCP_REPROG_PORT_OFFSET				((uint16_t) (+4))
#define UART1_PORT_OFFSET					((uint16_t) (+1))
#define CTRL_PORT_TCP_OFFSET				((uint16_t) (+6))
#define CTRL_PORT_UDP_OFFSET				((uint16_t) (+7))

extern unsigned char RPSPI_ReceiveBuffer[];

// *****************************************************************************
extern uint32_t socketHandleTCP0, socketHandleUDP0, socketHandleTCPRP0;
extern uint32_t ipAddressMemory0;
extern uint32_t uart0ActiveSocket;

extern uint32_t socketHandleTCP1, socketHandleUDP1, socketHandleTCPRP1;
extern uint32_t ipAddressMemory1;
extern uint32_t uart1ActiveSocket;

extern uint32_t socketHandleCTRLTCP, socketHandleCTRLUDP;
extern uint32_t ipAddressMemoryCTRL;

extern uint32_t myCurrentIPAddress;
extern uint32_t myMACAddress[];

extern uint32_t commandModeEchoActive;
char CTRLCmdLine[COMMAND_LINE_MAX_LENGTH];
uint32_t CTRLCmdLinePointer;
extern char commandReturn[];

#define CONNECT_TRIALS_PER_NETWORK		2			// try x times to connect before switching to next networkextern const char NetworkNameKeyType[][32];// *****************************************************************************
// ** Init System, called once on powerup **
// *****************************************************************************
void RP_Init(void) {
	SPI_Init();		 									   // init SPI interface
	CTRLCmdLinePointer = 0;

	myCurrentIPAddress = 0;

	myMACAddress[0] = 0;
	myMACAddress[1] = 0;
	myMACAddress[2] = 0;
	myMACAddress[3] = 0;
	myMACAddress[4] = 0;
	myMACAddress[5] = 0;
}

// *****************************************************************************
// ** Perform Hardware Reset **
// *****************************************************************************
void RP_HardwareReset(void) {
	//DEBUGOUT1("Resetting RP Module\n");
	Chip_GPIO_SetPinState(LPC_GPIO, 0, 17, 0); //set reset to low
	timerDelayMs(100);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, 17, 1); //set reset to high
	//DEBUGOUT1("done.\n");
}

// *****************************************************************************
// ** Boot Module **
// *****************************************************************************
int32_t RP_BootModule(void) {

	//DEBUGOUT1("RPBOOT: hardware reset of RP WLAN Module...\n");
	RP_HardwareReset();
	timerDelayMs(20);

	//DEBUGOUT1("RPBOOT: initializing SPI interface...\n");
	if (RPSPI_WakeupInterface() < 0) {
		return (-1);
	}
	timerDelayMs(10);

	//DEBUGOUT1("RPBOOT: loading boot images...\n");
	RPLoadBootImages();

	//DEBUGOUT1("RPBOOT: done!\n\n");
	return (0);
}

// *****************************************************************************
// ** Setup Module **
// *****************************************************************************
int32_t RP_InitModule(void) {
	int32_t n, m;
	char *nName, *nKey, *nKeyType;
	//DEBUGOUT1("Initializing Module\n");

#ifdef USE_28_MODULE
	//DEBUGOUT1("Calling MF_BAND\n");
	if (RP_MF_BAND(1) < 0) {
		//DEBUGOUT1("Can't set band to 5GHz, maybe not supported by module?");
	}
	timerDelayMs(20);
#endif

	//DEBUGOUT1("Calling MF_INIT\n");
	if (RP_MF_INIT() < 0)
		return (-1);
	timerDelayMs(20);

//  xprintf("size: %d\n", sizeof(NetworkNameKeyType));
	n = -1;

	while (1) {
		n++;
		nName = ((char *) NetworkNameKeyType) + (n * 3 * 32);
		if (*nName == ((char) 0)) {
			break;
		}
		nKey = nName + 32;
		nKeyType = nKey + 32;

		for (m = 0; m < CONNECT_TRIALS_PER_NETWORK; m++) {

			DEBUGOUT6("\n\nTrying (%d/%d) to connect to Network %s with key %s (%d)\n", m, CONNECT_TRIALS_PER_NETWORK, nName, nKey, ((*nKeyType)-'0'));

			//DEBUGOUT1("Calling MF_SCAN\n");
#ifdef NETWORK_CHANNEL
			if (RP_MF_SCAN(NETWORK_CHANNEL, nName) >= 0) {
#else
			if (RP_MF_SCAN(0, nName) >= 0) {
#endif
				timerDelayMs(20);

				//DEBUGOUT1("\n\nCalling MF_JOIN\n");
				if (RP_MF_JOIN(nName, ((*nKeyType) - '0'), nKey) >= 0) {
					timerDelayMs(20);

					//DEBUGOUT1("\n\nSetting up IP parameters, Addr: ");
#ifdef NETWORK_FIXED_IP_ADDR
					DEBUGOUT5("%3d.%3d.%3d.%3d\n", ((NETWORK_FIXED_IP_ADDR>>24)&0xFF), ((NETWORK_FIXED_IP_ADDR>>16)&0xFF), ((NETWORK_FIXED_IP_ADDR>>8)&0xFF), ((NETWORK_FIXED_IP_ADDR)&0xFF));
					if (RP_DF_TCPIP_FIXIPsimple_Config(NETWORK_FIXED_IP_ADDR) >= 0) {
#else
					//DEBUGOUT1("DHCP\n");
					if (RP_DF_TCPIP_DHCP_Config() >= 0) {
#endif

						socketHandleTCP0 = 0;
						socketHandleUDP0 = 0;
						socketHandleTCPRP0 = 0;
						socketHandleTCP1 = 0;
						socketHandleUDP1 =0;
						socketHandleTCPRP1 = 0;
						socketHandleCTRLTCP = 0;
						socketHandleCTRLUDP = 0;

						//DEBUGOUT1("\n\nInitModule done!\n\n");
						return (0);
					}
				}
			}
		}
	}
	return (-1);		  // failure after trying all networks!
}

// *****************************************************************************
extern void DEBUG_DisplayReceivedFrame(int32_t d);		// PROTOTYPE FOR DEBUG
void RP_InterruptDataAvailable(void) {
	int32_t count;
	int32_t socketHandle;
	int32_t reply;

	//DEBUGOUT1("RP - received unexpedted Frame, polling data:\n");

	reply = RPSPI_SPIRegisterRead(0x00);
	if ((reply & _BIT(3)) == 0) {		// is this interrupt really for waiting data?
		//DEBUGOUT1("RP - received unexpedted Frame, but interrupt flag states no data available. ignoring!\n");
		return;// xxx
	}

	// a truly "unexpected" frame must be a data frame, so go and fetch!
	count = RPSPI_DataFrameRead(RPSPI_ReceiveBuffer, 1600);

	if (count > 0) {

#ifdef DEBUG_MODE
		DEBUG_DisplayReceivedFrame(count);
#endif
		switch (RPSPI_ReceiveBuffer[16]) {

		// ************************************************************* create socket
//    case 0x02:	//DEBUGOUT1("Socket created:\n");
//		 		DEBUGOUT2("SocketType:           %d\n", *((uint16_t *) (((char *)RPSPI_ReceiveBuffer)+16+2)));
//		 		DEBUGOUT2("SocketHandle:         %d\n", *((uint16_t *) (((char *)RPSPI_ReceiveBuffer)+16+4)));
//				DEBUGOUT2("Local Port:          %d\n", *((uint16_t *) (((char *)RPSPI_ReceiveBuffer)+16+6)));
//				DEBUGOUT5("Local IP-Address:    %3d.%3d.%3d.%3d\n", RPSPI_ReceiveBuffer[16+ 8], RPSPI_ReceiveBuffer[16+ 9], RPSPI_ReceiveBuffer[16+10], RPSPI_ReceiveBuffer[16+11]);
//				DEBUGOUT2("ErrorCode:            %ld\n", *((int32_t *) (((char *)RPSPI_ReceiveBuffer)+16+12)));
//				DEBUGOUT2("MaxSegmentSize:       %d\n", *((uint16_t *) (((char *)RPSPI_ReceiveBuffer)+16+16)));
//				break;

		// ************************************************************* open socket
		case 0x04:	//DEBUGOUT1("Remote Connection opened socket:\n");
					DEBUGOUT2("Local Socket Handle:  %d\n", *((uint16_t *) (((char *)RPSPI_ReceiveBuffer)+16+2)));
					DEBUGOUT2("Remote Socket Port:   %d\n", *((uint16_t *) (((char *)RPSPI_ReceiveBuffer)+16+4)));
					DEBUGOUT5("Remote IP-Address:    %3d.%3d.%3d.%3d\n", RPSPI_ReceiveBuffer[16+ 6], RPSPI_ReceiveBuffer[16+ 7], RPSPI_ReceiveBuffer[16+ 8], RPSPI_ReceiveBuffer[16+ 9]);
		{					// this error code does not start at a 4bytes boundary, hence I need to "assemble" the value
			int32_t ec;
			ec = *(((char *) RPSPI_ReceiveBuffer) + 16 + 10);
			ec |= ((uint32_t) (*(((char *) RPSPI_ReceiveBuffer) + 16 + 11))) << 8;
			ec |= ((uint32_t) (*(((char *) RPSPI_ReceiveBuffer) + 16 + 12))) << 16;
			ec |= ((uint32_t) (*(((char *) RPSPI_ReceiveBuffer) + 16 + 13))) << 24;
			DEBUGOUT2("ErrorCode:            %ld\n", ec);
		}

			socketHandle = *((uint16_t *) (((char *) RPSPI_ReceiveBuffer) + 16 + 2));

			if (socketHandle == socketHandleTCP0) {
				uart0ActiveSocket = socketHandle;					// remember most recent connection on UART0
				uart0SetBaudRate(DEFAULT_BAUDRATE);	// set UART port to default baud rate
				uart0SetRTSCTSEnable(TRUE);	// enable RTS/CTS during normal operation
			}
//				if (socketHandle == socketHandleUDP0) {		// UDP does not negotiate on opening sockets, so no news here...
//				}
			if (socketHandle == socketHandleTCPRP0) {
				uart0ActiveSocket = socketHandle;	// remember most recent connection on UART0
				uart0SetBaudRate(REPROG_BAUDRATE);	// set UART port to reprogramming baud rate
				uart0SetRTSCTSEnable(FALSE);	// disable RTS/CTS during reprogramming
			}

			if (socketHandle == socketHandleTCP1) {
				uart1ActiveSocket = socketHandle;	// remember most recent connection on UART1
				uart1SetBaudRate(DEFAULT_BAUDRATE);// set UART port to default baud rate
				uart1SetRTSCTSEnable(TRUE);// enable RTS/CTS during normal operation
			}
//				if (socketHandle == socketHandleUDP1) {		// UDP does not negotiate on opening sockets, so no news here...
//				}
			if (socketHandle == socketHandleTCPRP1) {
				uart1ActiveSocket = socketHandle;	// remember most recent connection on UART1
				uart1SetBaudRate(REPROG_BAUDRATE);// set UART port to reprogramming baud rate
				uart1SetRTSCTSEnable(FALSE);// disable RTS/CTS during reprogramming
			}

			if (socketHandle == socketHandleCTRLTCP) {
				(void) RPSendTCPDataPacket(socketHandleCTRLTCP, WLAN_WELCOME_MESSAGE, strlen(WLAN_WELCOME_MESSAGE));
			}
			if (socketHandle == socketHandleCTRLUDP) {
				ipAddressMemoryCTRL = ULONG_IP_ADDRESS(RPSPI_ReceiveBuffer[16 + 6], RPSPI_ReceiveBuffer[16 + 7], RPSPI_ReceiveBuffer[16 + 8],
						RPSPI_ReceiveBuffer[16 + 9]);
				(void) RPSendUDPDataPacket(socketHandleCTRLUDP, ipAddressMemoryCTRL, BASE_PORT + CTRL_PORT_UDP_OFFSET, WLAN_WELCOME_MESSAGE,
						strlen(WLAN_WELCOME_MESSAGE));
			}

			break;

			// ************************************************************ close socket
		case 0x05:	//DEBUGOUT1("Remote Connection closed socket:\n");
					DEBUGOUT2("Local Socket Handle:  %d\n", *((uint16_t *) (((char *)RPSPI_ReceiveBuffer)+16+2)));
					DEBUGOUT2("ErrorCode:            %ld\n", *((int32_t *) (((char *)RPSPI_ReceiveBuffer)+16+4)));

			socketHandle = *((uint16_t *) (((char *) RPSPI_ReceiveBuffer) + 16 + 2));
			if (socketHandle == socketHandleTCP0) {
				socketHandleTCP0 = 0;	  	// -> will be reopened in main loop
				if (socketHandle == uart0ActiveSocket)
					uart0ActiveSocket = 0;
			}
//				if (socketHandle == socketHandleUDP0) {	 	// UDP does not negotiate on socket close
//				  socketHandleUDP0 = 0;	  		 // -> will be reopened in main loop
//				  if (socketHandle == uart0ActiveSocket) uart0ActiveSocket = 0;
//				}
			if (socketHandle == socketHandleTCPRP0) {
				socketHandleTCPRP0 = 0;		 // -> will be reopened in main loop
				if (socketHandle == uart0ActiveSocket)
					uart0ActiveSocket = 0;
			}

			if (socketHandle == socketHandleTCP1) {
				socketHandleTCP1 = 0;	  	// -> will be reopened in main loop
				if (socketHandle == uart1ActiveSocket) uart1ActiveSocket = 0;
			}
//				if (socketHandle == socketHandleUDP1) {	 	// UDP does not negotiate on socket close
//				  socketHandleUDP1 = 0;	  		 // -> will be reopened in main loop
//				  if (socketHandle == uart1ActiveSocket) uart1ActiveSocket = 0;
//				}
			if (socketHandle == socketHandleTCPRP1) {
				socketHandleTCPRP1 = 0;		 // -> will be reopened in main loop
				if (socketHandle == uart1ActiveSocket) uart1ActiveSocket = 0;
			}

			if (socketHandle == socketHandleCTRLTCP) {
				socketHandleCTRLTCP = 0;	// -> will be reopened in main loop
			}
//				if (socketHandle == socketHandleCTRLUDP) {	// UDP does not negotiate on socket close
//				  socketHandleCTRLUDP=0;		 // -> will be reopened in main loop
//				}

			break;

			// ******************************************************** data from socket
		case 0x07: {	  			   					   	 	// received data
			uint32_t dataLength, dataOffset;
			socketHandle = *((uint16_t *) (((char *) RPSPI_ReceiveBuffer) + 16 + 2));
			dataLength = *((uint32_t *) (((char *) RPSPI_ReceiveBuffer) + 16 + 4));
			dataOffset = *((uint16_t *) (((char *) RPSPI_ReceiveBuffer) + 16 + 8));
			//DEBUGOUT1("Received Data:\n");
			DEBUGOUT2("Local Socket Handle:  %d\n", socketHandle);
			DEBUGOUT2("DataLength:           %ld\n", dataLength);
			DEBUGOUT2("DataOffset:           %d\n", dataOffset);
			DEBUGOUT2("In-Port:              %d\n", *((uint16_t *) (((char *)RPSPI_ReceiveBuffer)+16+10)));
			DEBUGOUT5("Remote IP-Address:    %3d.%3d.%3d.%3d\n", RPSPI_ReceiveBuffer[16+12], RPSPI_ReceiveBuffer[16+13], RPSPI_ReceiveBuffer[16+14], RPSPI_ReceiveBuffer[16+15]);

//				  //DEBUGOUT1("Data: ");
//				  DEBUG_DisplayReceivedFrame(count-16);
//				  //DEBUGOUT1("\n");

			// is this data for UART0?
			if ((socketHandle == socketHandleTCP0) || (socketHandle == socketHandleUDP0) || (socketHandle == socketHandleTCPRP0)) {

				if (socketHandle != uart0ActiveSocket) {			// oha... data from a different port... change settings!
					if ((socketHandle == socketHandleTCP0) || (socketHandle == socketHandleUDP0)) {
						uart0ActiveSocket = socketHandle; // remember most recent connection on UART0
						uart0SetBaudRate(DEFAULT_BAUDRATE);	// set UART0 to default baud rate
						uart0SetRTSCTSEnable(TRUE);	// enable RTS/CTS during normal operation
					}
					if (socketHandle == socketHandleTCPRP0) {
						uart0ActiveSocket = socketHandle; // remember most recent connection on UART0
						uart0SetBaudRate(REPROG_BAUDRATE); // set UART0 to reprogramming baud rate
						uart0SetRTSCTSEnable(FALSE); // disable RTS/CTS during reprogramming
					}
				}

				if (socketHandle == socketHandleUDP0) {
					ipAddressMemory0 = ULONG_IP_ADDRESS(RPSPI_ReceiveBuffer[16 + 12], RPSPI_ReceiveBuffer[16 + 13], RPSPI_ReceiveBuffer[16 + 14],
							RPSPI_ReceiveBuffer[16 + 15]);
				}

				uart0SendString(RPSPI_ReceiveBuffer + 16 + dataOffset, dataLength);

			} // endif data for UART0

			// is this data for UART1?
			if ((socketHandle == socketHandleTCP1) || (socketHandle == socketHandleUDP1) || (socketHandle == socketHandleTCPRP1)) {

				if (socketHandle != uart1ActiveSocket) { // oha... data from a different port... change settings!
					if ((socketHandle == socketHandleTCP1) || (socketHandle == socketHandleUDP1)) {
						uart1ActiveSocket = socketHandle; // remember most recent connection on UART1
						uart1SetBaudRate(DEFAULT_BAUDRATE); // set UART1 to default baud rate
						uart1SetRTSCTSEnable(TRUE); // enable RTS/CTS during normal operation
					}
					if (socketHandle == socketHandleTCPRP1) {
						uart1ActiveSocket = socketHandle; // remember most recent connection on UART1
						uart1SetBaudRate(REPROG_BAUDRATE); // set UART1 to reprogramming baud rate
						uart1SetRTSCTSEnable(FALSE); // disable RTS/CTS during reprogramming
					}
				}

				if (socketHandle == socketHandleUDP1) {
					ipAddressMemory1 = ULONG_IP_ADDRESS(RPSPI_ReceiveBuffer[16 + 12], RPSPI_ReceiveBuffer[16 + 13], RPSPI_ReceiveBuffer[16 + 14],
							RPSPI_ReceiveBuffer[16 + 15]);
				}

				uart1SendString(RPSPI_ReceiveBuffer + 16 + dataOffset, dataLength);

			} // endif data for UART1

			if (socketHandle == socketHandleCTRLTCP) {
				unsigned char *data = RPSPI_ReceiveBuffer + 16 + dataOffset;
				int32_t l = dataLength;
				for (; l; l--) {
					switch (*data) {
					case 8:			// backspace
						if (CTRLCmdLinePointer > 0) {
							CTRLCmdLinePointer--;
							if (commandModeEchoActive) {
								(void) RPSendTCPDataPacket(socketHandleCTRLTCP, "\b \b", 3);
							}
						}
						break;

					case 10:
					case 13:
						if (commandModeEchoActive) {
							(void) RPSendTCPDataPacket(socketHandleCTRLTCP, "\n", 1);
						}
						if (CTRLCmdLinePointer > 0) {
							CTRLCmdLine[CTRLCmdLinePointer] = 0;
							if (parseCommand(CTRLCmdLine)) {
								(void) RPSendTCPDataPacket(socketHandleCTRLTCP, commandReturn, strlen(commandReturn));
							}
							CTRLCmdLinePointer = 0;
						}
						break;

					default:
						if (CTRLCmdLinePointer < (COMMAND_LINE_MAX_LENGTH - 2)) {
							if (commandModeEchoActive) {
								(void) RPSendTCPDataPacket(socketHandleCTRLTCP, (char*)data, 1);
							}
							CTRLCmdLine[CTRLCmdLinePointer] = *data;
							CTRLCmdLinePointer++;
						}
					}  // end of switch
					data++;
				}
			}

			if (socketHandle == socketHandleCTRLUDP) {
				unsigned char *data = RPSPI_ReceiveBuffer + 16 + dataOffset;
				int32_t l = dataLength;
				for (; l; l--) {
					switch (*data) {
					case 8:			// backspace
						if (CTRLCmdLinePointer > 0) {
							CTRLCmdLinePointer--;
							(void) RPSendUDPDataPacket(socketHandleCTRLUDP, ipAddressMemoryCTRL, BASE_PORT + CTRL_PORT_UDP_OFFSET, "\b \b", 3);
						}
						break;

					case 10:
					case 13:
						(void) RPSendUDPDataPacket(socketHandleCTRLUDP, ipAddressMemoryCTRL, BASE_PORT + CTRL_PORT_UDP_OFFSET, "\n", 1);
						if (CTRLCmdLinePointer > 0) {
							CTRLCmdLine[CTRLCmdLinePointer] = 0;
							if (parseCommand(CTRLCmdLine)) {
								(void) RPSendUDPDataPacket(socketHandleCTRLUDP, ipAddressMemoryCTRL, BASE_PORT + CTRL_PORT_UDP_OFFSET, commandReturn,
										strlen(commandReturn));
							}
							CTRLCmdLinePointer = 0;
						}
						break;

					default:
						if (CTRLCmdLinePointer < (COMMAND_LINE_MAX_LENGTH - 2)) {
							(void) RPSendUDPDataPacket(socketHandleCTRLUDP, ipAddressMemoryCTRL, BASE_PORT + CTRL_PORT_UDP_OFFSET, (char*)data, 1);
							CTRLCmdLine[CTRLCmdLinePointer] = *data;
							CTRLCmdLinePointer++;
						}
					}  // end of switch
					data++;
				}
//					uart0SendString("Rec Data UDP: ", 14);
//					uart0SendString(RPSPI_ReceiveBuffer+16+dataOffset, dataLength);
			}

		}
			break;

			// *********************************************************** unknown event
		default:    //DEBUGOUT1("Unknown frame. Framedump:\n");
#ifdef DEBUG_MODE
		DEBUG_DisplayReceivedFrame(count-16);
#endif
			break;
		}
	}
}

// *****************************************************************************
void RP_ModuleFWVersion(char *fwString) {
	RP_DF_QUERY_FIRMWARE_VERSION(fwString);
}

// *****************************************************************************
void RP_i0(void) {
//  RP_MF_SCAN(0, "");
	RP_DF_OPEN_TCP_LISTENER(BASE_PORT);
}

// *****************************************************************************
void RP_i1(void) {
//  RP_MF_JOIN("Cotesys", 2, "5vhj67vn43efds34refui7ztg");
	RPSendTCPDataPacket(1, "12345678", 1);
}

// *****************************************************************************
void RP_i2(void) {
//  RP_DF_TCPIP_DHCP_Config();
	RPSendTCPDataPacket(1, "12345678", 2);
}

// *****************************************************************************
void RP_i3(void) {
//  RP_DF_OPEN_TCP_LISTENER(BASE_PORT);
	RPSendTCPDataPacket(1, "12345678", 3);
}

// *****************************************************************************
void RP_i4(void) {
//  RP_DF_OPEN_TCP_LISTENER(BASE_PORT+1);
	RPSendTCPDataPacket(1, "12345678", 4);
}

// *****************************************************************************
void RP_i5(void) {
	RPSendTCPDataPacket(1, "12345678", 5);
}

// *****************************************************************************
void RP_i6(void) {
	RPSendTCPDataPacket(1, "1234567890", 6);
}

// *****************************************************************************
void RP_i7(void) {
	RPSendTCPDataPacket(1, "1234567890AB", 7);
}

// *****************************************************************************
void RP_i8(void) {
	RPSendTCPDataPacket(1, "1234567890AB", 8);
//  (void) RP_DF_CLOSE_SOCKET(1);
}

// *****************************************************************************
void RP_i9(void) {
	RPSendTCPDataPacket(1, "1234567890AB", 9);
//  RP_DF_QUERY_FIRMWARE_VERSION();
}
