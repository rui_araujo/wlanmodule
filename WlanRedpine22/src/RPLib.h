/*
 * RPLib.h
 *
 *  Created on: Jun 10, 2014
 *      Author: raraujo
 */

#ifndef RPLIB_H_
#define RPLIB_H_
#include <stdint.h>

extern int32_t RP_MF_BAND(uint32_t band);	  		// management commands
extern int32_t RP_MF_INIT(void);
extern int32_t RP_MF_SCAN(uint32_t channelID, char * SSID);
extern int32_t RP_MF_JOIN(char *SSID, unsigned char secMode, char *PSK);

extern int32_t RP_DF_TCPIP_DHCP_Config(void);		  // data commands
extern int32_t RP_DF_TCPIP_FIXIP_Config(uint32_t myIPAddress, uint32_t NetMask, uint32_t DefGateWay, uint32_t NameServer);
extern int32_t RP_DF_TCPIP_FIXIPsimple_Config(uint32_t myIPAddress);
extern int32_t RP_DF_OPEN_TCP_LISTENER(uint16_t portNumber);
extern int32_t RP_DF_OPEN_TCP_CLIENT(uint16_t localPortNumber, uint16_t remotePortNumber, uint32_t remoteIPAddress);
extern int32_t RP_DF_OPEN_UDP_LISTENER(uint16_t portNumber);
extern int32_t RP_DF_OPEN_UDP_CLIENT(uint16_t localPortNumber, uint16_t remotePortNumber, uint32_t remoteIPAddress);
extern int32_t RP_DF_CLOSE_SOCKET(uint16_t socketNumber);
extern int32_t RP_DF_QUERY_FIRMWARE_VERSION(char *fwString);

extern int32_t RPSendTCPDataPacket(uint32_t socketNumber, char *data, int32_t dataLength);
extern int32_t RPSendUDPDataPacket(uint32_t socketNumber, uint32_t destIPAddress, uint32_t destPort, char *data, int32_t dataLength);

extern int32_t RPLoadBootImages(void);			  // bootup commands

#endif /* RPLIB_H_ */
