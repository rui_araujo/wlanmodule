/*
 * rp_spi_driver.h
 *
 *  Created on: Jun 4, 2014
 *      Author: raraujo
 */

#ifndef RP_SPI_DRIVER_H_
#define RP_SPI_DRIVER_H_
#include <stdint.h>
#include "chip.h"

// *****************************************************************************
// ** Response Data from RedPine Module
// *****************************************************************************
#define RPSPI_RESPONSE_OK			(0x58)
#define RPSPI_RESPONSE_FAIL			(0x52)
#define RPSPI_RESPONSE_STARTTOKEN	(0x55)
#define RPSPI_RESPONSE_BUSY			(0x54)

#ifdef DEBUG_MODE
#include "xprintf.h"
#include "UART.h"
#define DEBUGOUT1(a)				{xprintf(a); uart0InitiateInterruptSending();}
#define DEBUGOUT2(a,b)				{xprintf(a,b); uart0InitiateInterruptSending();}
#define DEBUGOUT3(a,b,c)			{xprintf(a,b,c); uart0InitiateInterruptSending();}
#define DEBUGOUT4(a,b,c,d)			{xprintf(a,b,c,d); uart0InitiateInterruptSending();}
#define DEBUGOUT5(a,b,c,d,e)		{xprintf(a,b,c,d,e); uart0InitiateInterruptSending();}
#define DEBUGOUT6(a,b,c,d,e,f)		{xprintf(a,b,c,d,e,f); uart0InitiateInterruptSending();}
#define DEBUGOUT7(a,b,c,d,e,f,g)	{xprintf(a,b,c,d,e,f,g); uart0InitiateInterruptSending();}
#else
#define C(a)				{}
#define DEBUGOUT1(a)				{}
#define DEBUGOUT2(a,b)				{}
#define DEBUGOUT3(a,b,c)			{}
#define DEBUGOUT4(a,b,c,d)			{}
#define DEBUGOUT5(a,b,c,d,e)		{}
#define DEBUGOUT6(a,b,c,d,e,f)		{}
#define DEBUGOUT7(a,b,c,d,e,f,g)	{}
#endif


// *****************************************************************************
extern unsigned char RPSPI_FrameHeader[16];
extern unsigned char RPSPI_DataFrameCommandHeader[2];
extern unsigned char RPSPI_TransmitBuffer[1600];
extern unsigned char RPSPI_ReceiveBuffer[1600];

extern void SPI_Init(void);
extern int32_t RPSPI_WakeupInterface(void);

extern unsigned char RPSPI_exchange(unsigned char d_out);

extern int32_t RPSPI_SPIRegisterWrite(uint32_t offset, uint32_t data);
extern int32_t RPSPI_SPIRegisterRead(uint32_t offset);

extern int32_t RPSPI_MemoryWrite(uint32_t addr, unsigned char *data, uint32_t dataLength);

extern int32_t RPSPI_FrameWrite(uint32_t cmd, unsigned char *data, uint32_t dataLength);	 // required for FWUpgrade
extern int32_t RPSPI_ManagementFrameWrite(unsigned char frameType, unsigned char *data, uint32_t dataLength);
extern int32_t RPSPI_DataFrameWrite(unsigned char frameType, unsigned char *data, uint32_t dataLength);

extern int32_t RPSPI_ManagementFrameRead(unsigned char *dataSpace, uint32_t maxDataLength);
extern int32_t RPSPI_DataFrameRead(unsigned char *dataSpace, uint32_t maxDataLength);


#endif /* RP_SPI_DRIVER_H_ */
