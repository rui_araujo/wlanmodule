#ifdef INCLUDE_RP_FIRMWARE_UPGRADE
#include <string.h>	   			// for memset#include <stdint.h>#include "rp_spi_driver.h"#include "utils.h"#include "UART.h"
#include "xprintf.h"
#include "RPinterface.h"
#include "rp_pinout.h"
// ******************************************************************** FW 4-7-4  for -28 module
#ifdef USE_28_MODULE
#define FFDATA_LENGTH		((uint32_t) 58088)		// in lines (== bytes)#define FFINST1_LENGTH  	((uint32_t) 65152)		// in lines (== bytes)#define FFINST2_LENGTH  	((uint32_t) 30532)		// in lines (== bytes)const char iuinst1[]= {#include "Firmware-28/iuinst1"};const char iuinst2[]= {
#include "Firmware-28/iuinst2"
};
const char iudata[]= {
#include "Firmware-28/iudata"
};
#endif

// ******************************************************************** FW 4-7-4  for -22 module
#ifdef USE_22_MODULE
#define FFDATA_LENGTH		((uint32_t) 58140)		// in lines (== bytes)#define FFINST1_LENGTH  	((uint32_t) 65096)		// in lines (== bytes)#define FFINST2_LENGTH  	((uint32_t) 29584)		// in lines (== bytes)const char iuinst1[] = {#include "Firmware-22/iuinst1"};const char iuinst2[] = {
#include "Firmware-22/iuinst2"
		};
const char iudata[] = {
#include "Firmware-22/iudata"
		};
#endif

const char ffinst1[] = {
#include "Firmware-22/ffinst1"
		};
const char ffinst2[] = {
#include "Firmware-22/ffinst2"
		};
const char ffdata[] = {
#include "Firmware-22/ffdata"
		};

extern char RPSPI_FrameHeader[];
extern char RPSPI_DataFrameCommandHeader[];
extern uint8_t RPSPI_TransmitBuffer[];
extern uint8_t RPSPI_ReceiveBuffer[];

// *****************************************************************************
// ** Firmware Upgrade - receive data over UART **
// *****************************************************************************
int32_t RPSPI_FirmwareUpgradeReceiveUARTDataAndPerformMemoryWrite(uint32_t addr, uint32_t dataLength) {
	unsigned char reply;
	uint32_t cmd;

	int32_t n, p;
	unsigned char inBuffer[12], c, d;

	if ((dataLength & 0x03) != 0) {  				// is legnth a data a multiple of four bytes?
		xprintf("SPIUARTMemWrite requires multiple of 4 bytes!\n");
		uart0InitiateInterruptSending();
		return (-1);
	}

	cmd = 0x0474;								// -> write memory
	cmd |= (dataLength & 0xFFFF) << 16;			// length msb,lsb

	xprintf("SPIUARTMemWrite, sending cmd header\n");
	uart0InitiateInterruptSending();

	RPSPI_exchange(cmd & 0xFF);					// send lowest byte

	cmd >>= 8;									// prepare and send next byte
	if ((reply = RPSPI_exchange(cmd & 0xFF)) != RPSPI_RESPONSE_OK) {
		xprintf("error SPIUARTMemWrite (C2): 0x%02X!\n", reply);
		uart0InitiateInterruptSending();
		return (-1);
	}

	cmd >>= 8;									// prepare and send next byte
	if ((reply = RPSPI_exchange(cmd & 0xFF)) != RPSPI_RESPONSE_OK) {
		xprintf("error SPIUARTMemWrite (C3): 0x%02X!\n", reply);
		uart0InitiateInterruptSending();
		return (-2);
	}

	cmd >>= 8;									// prepare and send next byte
	if ((reply = RPSPI_exchange(cmd & 0xFF)) != RPSPI_RESPONSE_OK) {
		xprintf("error SPIUARTMemWrite (C4): 0x%02X!\n", reply);
		uart0InitiateInterruptSending();
		return (-3);
	}

	xprintf("SPIUARTMemWrite, sending address\n");
	uart0InitiateInterruptSending();
// send lowest byte
	if ((reply = RPSPI_exchange(addr & 0xFF)) != RPSPI_RESPONSE_OK) {
		xprintf("error SPIUARTMemWrite (A0): 0x%02X!\n", reply);
		uart0InitiateInterruptSending();
		return (-1);
	}

	addr >>= 8;									// prepare and send next byte
	if ((reply = RPSPI_exchange(addr & 0xFF)) != RPSPI_RESPONSE_OK) {
		xprintf("error SPIUARTMemWrite (A1): 0x%02X!\n", reply);
		uart0InitiateInterruptSending();
		return (-1);
	}

	addr >>= 8;									// prepare and send next byte
	if ((reply = RPSPI_exchange(addr & 0xFF)) != RPSPI_RESPONSE_OK) {
		xprintf("error SPIUARTMemWrite (A2): 0x%02X!\n", reply);
		uart0InitiateInterruptSending();
		return (-2);
	}

	addr >>= 8;									// prepare and send next byte
	if ((reply = RPSPI_exchange(addr & 0xFF)) != RPSPI_RESPONSE_OK) {
		xprintf("error SPIUARTMemWrite (A3): 0x%02X!\n", reply);
		uart0InitiateInterruptSending();
		return (-3);
	}

	xprintf("SPIUARTMemWrite, starting payload\n");
	uart0InitiateInterruptSending();
	timerDelayMs(100);

	for (n = 0; n < dataLength; n += 0x0400) {			// show how much data we're expecting
		while ((LPC_UART0->LSR & _BIT(5)) == 0) {
		};
		LPC_UART0->THR = '.';
	}
	while ((LPC_UART0->LSR & _BIT(5)) == 0) {
	};
	LPC_UART0->THR = '.';  		  			 			// one more dot for line 1
	while ((LPC_UART0->LSR & _BIT(5)) == 0) {
	};
	LPC_UART0->THR = '.';  		  			 			// one more dot for last line
	while ((LPC_UART0->LSR & _BIT(5)) == 0) {
	};
	LPC_UART0->THR = '\n';

	n = 0;
	do {
		// get one "line" from UART
		p = 0;
		do {
			while ((LPC_UART0->LSR & 0x01) == 0) {
			};				// wait for input char on UART
			inBuffer[p] = LPC_UART0->RBR;						// fetch char
			p++;
			if (p == 8)
				inBuffer[7] = '\n';				// force end on int32_t lines!
		} while ((inBuffer[p - 1] != '\n') && (inBuffer[p - 1] != '\r'));    // end of line?

		if ((p == 6) || (p == 5)) {		  	 						// only lines with exactly 6 chars are valid: "0x00,\n"
			if ((inBuffer[0] == '0') && (inBuffer[1] == 'x')) {	 // check syntax

				c = inBuffer[2];
				if ((c >= '0') && (c <= '9')) {
					d = c - '0';
				} else {
					if ((c >= 'A') && (c <= 'F')) {
						d = 10 + c - 'A';
					} else {
						if ((c >= 'a') && (c <= 'f')) {
							d = 10 + c - 'a';
						} else {
							continue;
						}
					}
				}
				d <<= 4;

				c = inBuffer[3];
				if ((c >= '0') && (c <= '9')) {
					d += c - '0';
				} else {
					if ((c >= 'A') && (c <= 'F')) {
						d += 10 + c - 'A';
					} else {
						if ((c >= 'a') && (c <= 'f')) {
							d += 10 + c - 'a';
						} else {
							continue;
						}
					}
				}

				n++;
//		if ((n==1) || (n==dataLength) || ((n & 0x0FFF) == 0)) {xprintf(":%05X/%05X=%02X\n", n, dataLength, d);}
				if ((n == 1) || (n == dataLength) || ((n & 0x03FF) == 0)) {
					LPC_UART0->THR = '.';
				}

				reply = RPSPI_exchange(d);

			} else {
				xprintf(":-synterr\n");
				uart0InitiateInterruptSending();
			}
		} else {
			xprintf(":-ll!=6\n");
			uart0InitiateInterruptSending();
		}

	} while (n < dataLength);
	LPC_UART0->THR = '\n';

	xprintf("SPIUARTMemWrite, done\n");
	uart0InitiateInterruptSending();

	return (0);
}

// *****************************************************************************
// ** Firmware Upgrade - we need a special frame write command **
// **  (encoding data length of firmware packets) **
// *****************************************************************************
int32_t RPSPI_ManagementFrameWriteFUpgrade(unsigned char frameType, uint32_t dataLength) {
	uint32_t cmd;
	int32_t reply;

	xprintf("SPIManagementFrameWrite:\n");
	uart0InitiateInterruptSending();

	cmd = 0x0010047C;							// -> write management package, only header!

	memset(RPSPI_FrameHeader, 0, 16);			// clear SPIRP_FrameHeader, construct new header
	RPSPI_FrameHeader[1] = frameType;				// W0 [15:8], command
	RPSPI_FrameHeader[2] = dataLength & 0xFF;   	  	// W1 [ 7:0], number of bytes, low byte
	RPSPI_FrameHeader[3] = (dataLength & 0xFF00) >> 8;	// W1 [15:8], number of bytes, high byte
	RPSPI_FrameHeader[14] = 0x04;						// W7 [ 7:0], management frame

	reply = RPSPI_FrameWrite(cmd, NULL, 0);
	return (reply);
}

// *****************************************************************************
// ** Firmware Upgrade - this is the upgrade process **
// *****************************************************************************
void RP_FirmwareUpgrade(void) {
	uint32_t d;

	xprintf("Upgrading Firmware!...\n");
	uart0InitiateInterruptSending();

// **************************************************************** SPI Init (0)
	xprintf("spiRP: initialize SPI interface...");
	uart0InitiateInterruptSending();
	(void) RPSPI_exchange(0x15);
	(void) RPSPI_exchange(0x00);
	xprintf("done!\n");
	uart0InitiateInterruptSending();

	timerDelayMs(10);
// **************************************************** module in soft-reset (1)
	xprintf("spiRP: writing 0x00000001 to adr 0x22000004...");
	uart0InitiateInterruptSending();
	d = 0x00000001;
	(void) RPSPI_MemoryWrite(0x22000004, (uint8_t *) &d, 4);
	xprintf("done!\n");
	uart0InitiateInterruptSending();

	timerDelayMs(10);
// ********************************************************* loading iuinst1 (2)
	xprintf("spiRP: loading iuinst1 to adr 0x00000000...");
	uart0InitiateInterruptSending();
	(void) RPSPI_MemoryWrite(0x00000000, (uint8_t *) iuinst1, sizeof(iuinst1));
	xprintf("done!\n");
	uart0InitiateInterruptSending();

	timerDelayMs(10);
// ********************************************************* loading iuinst2 (3)
	xprintf("spiRP: loading iuinst2 to adr 0x02000000...");
	uart0InitiateInterruptSending();
	(void) RPSPI_MemoryWrite(0x02000000, (uint8_t *) iuinst2, sizeof(iuinst2));
	xprintf("done!\n");
	uart0InitiateInterruptSending();

	timerDelayMs(10);
// ********************************************************** loading iudata (4)
	xprintf("spiRP: loading iudata to adr 0x20003100...");
	uart0InitiateInterruptSending();
	(void) RPSPI_MemoryWrite(0x20003100, (uint8_t *) iudata, sizeof(iudata));
	xprintf("done!\n");
	uart0InitiateInterruptSending();

	timerDelayMs(10);
// ************************************************ module out of soft-reset (5)
	xprintf("spiRP: writing 0x00000000 to adr 0x22000004...");
	uart0InitiateInterruptSending();
	d = 0x00000000;
	(void) RPSPI_MemoryWrite(0x22000004, (uint8_t *) &d, 4);
	xprintf("done!\n");
	uart0InitiateInterruptSending();

	timerDelayMs(10);
// ******************************************************** read card status (6)
	xprintf("spiRP: waiting for card ready...\n");
	uart0InitiateInterruptSending();
	while (Chip_GPIO_ReadPortBit(LPC_GPIO, INTERRUPT_PORT, INTERRUPT_PIN) == 0) {
	};
	(void) RPSPI_ManagementFrameRead(RPSPI_ReceiveBuffer, 16);
	if (RPSPI_ReceiveBuffer[0] != 0x00) {
		xprintf("error in reply W0[7:0], expected 0x00, received 0x%02X\n", RPSPI_ReceiveBuffer[0]);
		uart0InitiateInterruptSending();
		return;
	}
	if (RPSPI_ReceiveBuffer[1] != 0x89) {
		xprintf("error in reply W0[15:8], expected 0x89, received 0x%02X\n", RPSPI_ReceiveBuffer[1]);
		uart0InitiateInterruptSending();
		return;
	}
	xprintf("done!\n");
	uart0InitiateInterruptSending();

	timerDelayMs(10);
// ************************************************************ load ffinst1 (7)
	xprintf("\n");
	uart0InitiateInterruptSending();
	xprintf("please send file FFINST1!\n");
	uart0InitiateInterruptSending();
	if (RPSPI_FirmwareUpgradeReceiveUARTDataAndPerformMemoryWrite(0x02008000, FFINST1_LENGTH) < 0) {
		xprintf("error receiving file FFINST1!\n");
		uart0InitiateInterruptSending();
		return;
	}
	xprintf("received file, sending ffinst1 upgrade command...\n");
	uart0InitiateInterruptSending();
	(void) RPSPI_ManagementFrameWriteFUpgrade(0x13, FFINST1_LENGTH);  // "FFINST1 UPGRADE"

	xprintf("spiRP: waiting for card ready...\n");
	uart0InitiateInterruptSending();
	while (Chip_GPIO_ReadPortBit(LPC_GPIO, INTERRUPT_PORT, INTERRUPT_PIN) == 0) {
	};
	(void) RPSPI_ManagementFrameRead(RPSPI_ReceiveBuffer, 16);

	if (RPSPI_ReceiveBuffer[0] != 0x00) {
		xprintf("error in reply W0[7:0], expected 0x00, received 0x%02X\n", RPSPI_ReceiveBuffer[0]);
		uart0InitiateInterruptSending();
		return;
	}
	if (RPSPI_ReceiveBuffer[1] != 0x91) {
		xprintf("error in reply W0[15:8], expected 0x91, received 0x%02X\n", RPSPI_ReceiveBuffer[1]);
		uart0InitiateInterruptSending();
		return;
	}
	xprintf("done with FFINST11!\n");
	uart0InitiateInterruptSending();

	timerDelayMs(10);
// ************************************************************ load ffinst2 (8)
	xprintf("\n");
	uart0InitiateInterruptSending();
	xprintf("please send file FFINST2!\n");
	uart0InitiateInterruptSending();
	if (RPSPI_FirmwareUpgradeReceiveUARTDataAndPerformMemoryWrite(0x02008000, FFINST2_LENGTH) < 0) {
		xprintf("error receiving file FFINST2!\n");
		uart0InitiateInterruptSending();
		return;
	}

	xprintf("received file, sending ffinst2 upgrade command...\n");
	uart0InitiateInterruptSending();
	(void) RPSPI_ManagementFrameWriteFUpgrade(0x14, FFINST2_LENGTH);  // "FFINST2 UPGRADE"

	xprintf("spiRP: waiting for card ready...\n");
	uart0InitiateInterruptSending();
	while (Chip_GPIO_ReadPortBit(LPC_GPIO, INTERRUPT_PORT, INTERRUPT_PIN) == 0) {
	};
	(void) RPSPI_ManagementFrameRead(RPSPI_ReceiveBuffer, 16);

	if (RPSPI_ReceiveBuffer[0] != 0x00) {
		xprintf("error in reply W0[7:0], expected 0x00, received 0x%02X\n", RPSPI_ReceiveBuffer[0]);
		uart0InitiateInterruptSending();
		return;
	}
	if (RPSPI_ReceiveBuffer[1] != 0x92) {
		xprintf("error in reply W0[15:8], expected 0x92, received 0x%02X\n", RPSPI_ReceiveBuffer[1]);
		uart0InitiateInterruptSending();
		return;
	}
	xprintf("done with FFINST2!\n");
	uart0InitiateInterruptSending();

	timerDelayMs(10);
// ************************************************************* load ffdata (9)
	xprintf("\n");
	uart0InitiateInterruptSending();
	xprintf("please send file FFDATA!\n");
	uart0InitiateInterruptSending();
	if (RPSPI_FirmwareUpgradeReceiveUARTDataAndPerformMemoryWrite(0x02008000, FFDATA_LENGTH) < 0) {
		xprintf("error receiving file FFDATA!\n");
		uart0InitiateInterruptSending();
		return;
	}
	xprintf("received file, sending ffdata upgrade command...\n");
	uart0InitiateInterruptSending();
	(void) RPSPI_ManagementFrameWriteFUpgrade(0x15, FFDATA_LENGTH);  // "FFDATA UPGRADE"

	xprintf("spiRP: waiting for card ready...\n");
	uart0InitiateInterruptSending();
	while (Chip_GPIO_ReadPortBit(LPC_GPIO, INTERRUPT_PORT, INTERRUPT_PIN) == 0) {
	};
	(void) RPSPI_ManagementFrameRead(RPSPI_ReceiveBuffer, 16);

	if (RPSPI_ReceiveBuffer[0] != 0x00) {
		xprintf("error in reply W0[7:0], expected 0x00, received 0x%02X\n", RPSPI_ReceiveBuffer[0]);
		uart0InitiateInterruptSending();
		return;
	}
	if (RPSPI_ReceiveBuffer[1] != 0x93) {
		xprintf("error in reply W0[15:8], expected 0x93, received 0x%02X\n", RPSPI_ReceiveBuffer[1]);
		uart0InitiateInterruptSending();
		return;
	}
	xprintf("done with FFDATA!\n");
	uart0InitiateInterruptSending();

	timerDelayMs(10);
// ************************************** firmware updgrade done, powercycle!!
	xprintf("\n");
	uart0InitiateInterruptSending();
//  xprintf("firmware upgrade complete --- PLEASE POWERCYCLE MODULE!!!\n"); uart0InitiateInterruptSending();
//  while(1) {}; 	   		   // force user to powercycle

	xprintf("firmware upgrade complete --- resetting module!!!\n");
	uart0InitiateInterruptSending();
	xprintf("\n");
	uart0InitiateInterruptSending();

	timerDelayMs(100);
	RP_HardwareReset();
	timerDelayMs(100);

	xprintf("firmware upgrade complete --- resetting microcontroller!!!\n");
	uart0InitiateInterruptSending();
	xprintf("\n");
	uart0InitiateInterruptSending();
	timerDelayMs(300);

	resetDevice();
}
#endif
