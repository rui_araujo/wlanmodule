/*
 * uart_pinout.h
 *
 *  Created on: Jun 10, 2014
 *      Author: raraujo
 */

#ifndef UART_PINOUT_H_
#define UART_PINOUT_H_

#define UART0_RX_PORT		0
#define UART0_RX_PIN		3

#define UART0_TX_PORT		0
#define UART0_TX_PIN		2

#define UART0_RTS_PORT		2
#define UART0_RTS_PIN		2

#define UART0_CTS_PORT		2
#define UART0_CTS_PIN		0

#define UART2_RX_PORT		0
#define UART2_RX_PIN		11

#define UART2_TX_PORT		0
#define UART2_TX_PIN		10

#define UART2_RTS_PORT		0
#define UART2_RTS_PIN		22

#define UART2_CTS_PORT		0
#define UART2_CTS_PIN		1


#endif /* UART_PINOUT_H_ */
