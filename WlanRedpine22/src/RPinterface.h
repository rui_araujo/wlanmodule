/*
 * RPinterface.h
 *
 *  Created on: Jun 10, 2014
 *      Author: raraujo
 */

#ifndef RPINTERFACE_H_
#define RPINTERFACE_H_
#include <stdint.h>

// *****************************************************************************
#define COMMAND_LINE_MAX_LENGTH			96
#define RETURN_LINE_MAX_LENGTH			(16*80)
#define BASE_PORT							((unsigned short) 56000)


#define ULONG_IP_ADDRESS(a,b,c,d)	((((uint32_t) a)<<24) | (((uint32_t) b)<<16) | (((uint32_t) c)<<8) | (((uint32_t) d)))

// RPInterface.c --- Functions to work with RP module
extern void RP_Init(void);	   	  	   		// called once for initialization

extern void RP_HardwareReset(void);
extern int32_t RP_BootModule(void);
extern int32_t RP_InitModule(void);

extern void RP_InterruptDataAvailable(void);

extern void RP_ModuleFWVersion(char *fwString);

extern void RP_i0(void);
extern void RP_i1(void);
extern void RP_i2(void);
extern void RP_i3(void);
extern void RP_i4(void);
extern void RP_i5(void);
extern void RP_i6(void);
extern void RP_i7(void);
extern void RP_i8(void);
extern void RP_i9(void);


#endif /* RPINTERFACE_H_ */
