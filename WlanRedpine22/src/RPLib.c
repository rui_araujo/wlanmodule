#include <string.h>			// for memset#include "RPLib.h"#include "RPinterface.h"#include "rp_pinout.h"#include "chip.h"#include "rp_spi_driver.h"#include "utils.h"extern unsigned char RPSPI_TransmitBuffer[];
extern unsigned char RPSPI_ReceiveBuffer[];

extern uint32_t myCurrentIPAddress;
extern uint32_t myMACAddress[];

// *****************************************************************************
// ** Firmware Data required for Bootup
// *****************************************************************************
#ifdef USE_22_MODULE
const char sbinst1[]= {
#include "Firmware-22/sbinst1"
};
const char sbinst2[]= {
#include "Firmware-22/sbinst2"
};
const char sbdata1[]= {
#include "Firmware-22/sbdata1"
};
const char sbdata2[]= {
#include "Firmware-22/sbdata2"
};
#endif

#ifdef USE_28_MODULE
const char sbinst1[]= {
#include "Firmware-28/sbinst1"
};
const char sbinst2[]= {
#include "Firmware-28/sbinst2"
};
const char sbdata1[]= {
#include "Firmware-28/sbdata1"
};
const char sbdata2[]= {
#include "Firmware-28/sbdata2"
};
#endif

// *****************************************************************************
// *****************************************************************************
void DEBUG_DisplayReceivedFrame(int32_t d) {
	int32_t n;

	DEBUGOUT1("Received header: ");
	for (n = 0; n < 16; n++) {
		DEBUGOUT2("%02X ", RPSPI_ReceiveBuffer[n]);
	}
	DEBUGOUT1("\n");
	DEBUGOUT1("Received data:");
	for (n = 16; n < 16 + d; n++) {
		if ((n & 0x0F) == 0) { DEBUGOUT2("\n[%3d]: ", n);
			DEBUGOUT2("%02X  ", RPSPI_ReceiveBuffer[n]);
		}
	}
	DEBUGOUT1("\n");
}

// *****************************************************************************
// ** Set of ManagementFrames **
// *****************************************************************************
int32_t RP_MF_BAND(uint32_t band) { // ********************************** BAND
	int32_t reply;

	DEBUGOUT1("MF_BAND called...\n");

	// Frame Body Payload				bytes		#of bytes		# total number bytes
//  UINT8  band_val					 0				1			  --> 1 --> 4

	memset(RPSPI_TransmitBuffer, 0, 4);	// clear SPIRP_TransmitBuffer (Payload)
	RPSPI_TransmitBuffer[0] = band;	// which band to select (0:2.4GHz;   1:5GHz)

	reply = RPSPI_ManagementFrameWrite(0x18, RPSPI_TransmitBuffer, 4);// command BAND, length of payload
	if (reply < 0) {
		return (-1);
	}

	DEBUGOUT1("MF_BAND: waiting for card ready...\n");
	while (Chip_GPIO_ReadPortBit(LPC_GPIO,INTERRUPT_PORT, INTERRUPT_PIN) == 0) {
	};
	if (RPSPI_ManagementFrameRead(RPSPI_ReceiveBuffer, 16) < 0) {
		return (-1);
	}
	if (RPSPI_ReceiveBuffer[1] != 0x97) {
		DEBUGOUT2("----- error in reply W0[15:8], expected 0x94, received 0x%02X\n", RPSPI_ReceiveBuffer[1]);
		return (-1);
	}
	DEBUGOUT1("done!\n");
	return (0);
}

int32_t RP_MF_INIT(void) { // ************************************************ INIT
	int32_t reply;

	DEBUGOUT1("MF_INIT called...\n");
	reply = RPSPI_ManagementFrameWrite(0x10, (unsigned char *) NULL, 0);
	if (reply < 0) {
		return (-1);
	}

	DEBUGOUT1("MF_INIT: waiting for card ready...\n");
	while (Chip_GPIO_ReadPortBit(LPC_GPIO,INTERRUPT_PORT, INTERRUPT_PIN) == 0) {
	};
	if (RPSPI_ManagementFrameRead(RPSPI_ReceiveBuffer, 16) < 0) {
		return (-1);
	}
	if (RPSPI_ReceiveBuffer[1] != 0x94) {
		DEBUGOUT2("----- error in reply W0[15:8], expected 0x94, received 0x%02X\n", RPSPI_ReceiveBuffer[1]);
		return (-1);
	}
	if (RPSPI_ReceiveBuffer[2] != 0x00) {
		DEBUGOUT2("----- error in reply W1[7:0], expected 0x00, received 0x%02X\n", RPSPI_ReceiveBuffer[2]);
		return (-1);
	}
	DEBUGOUT1("done!\n");
	return (0);
}

// *****************************************************************************
int32_t RP_MF_SCAN(uint32_t channelID, char *SSID) { // ********* SCAN
	int32_t reply;
	DEBUGOUT1("MF_SCAN called...\n");
	// Frame Body Payload				bytes		#of bytes		# total number bytes
//  UINT32 uChannelNo				 0.. 3	-> 		4
//  UINT8  uSSID[32]				 4..35	-> 		32	 		--> 36

	memset(RPSPI_TransmitBuffer, 0, 36);// clear SPIRP_TransmitBuffer (Payload)
	*((uint32_t *) RPSPI_TransmitBuffer) = channelID;// 0 --> all channels, otherwise restrict to channel #
	strcpy((char*)RPSPI_TransmitBuffer + 4, SSID); // 4..x: scan only for specific SSID

	reply = RPSPI_ManagementFrameWrite(0x11, RPSPI_TransmitBuffer, 36);	// command INIT, length of payload
	if (reply < 0) {
		return (-1);
	}

	DEBUGOUT1("MF_SCAN: waiting for card ready...\n");
	while (Chip_GPIO_ReadPortBit(LPC_GPIO,INTERRUPT_PORT, INTERRUPT_PIN) == 0) {
	};
	if (RPSPI_ManagementFrameRead(RPSPI_ReceiveBuffer, (16 + 4 + 4 + 32 * 35))
			< 0) {
		return (-1);
	}
	if (RPSPI_ReceiveBuffer[1] != 0x95) {
		DEBUGOUT2("----- error in reply W0[15:8], expected 0x95, received 0x%02X\n", RPSPI_ReceiveBuffer[1]);
		return (-1);
	}
	DEBUGOUT1("done!\n");
//  DEBUG_DisplayReceivedFrame(4+4+32*35);

#ifdef DEBUG_MODE
	{
		int32_t scanCount, n;
		char *p;
		scanCount = *(RPSPI_ReceiveBuffer+16);
		DEBUGOUT2("ScanCount: %ld\n", scanCount);
		DEBUGOUT1("    Channel#   SecMode   rssiVal   SSID\n");
		p=RPSPI_ReceiveBuffer+16+4+4;
		for (n=0; n<scanCount; n++) {
			DEBUGOUT2("        %2d", *(p+0) );
			DEBUGOUT2("        %2d", *(p+1) );
			DEBUGOUT2("        %2d", *(p+2) );
			DEBUGOUT2("        %s",   (p+3) );
			DEBUGOUT1("\n");
			p += 35;
		}
	}
#endif

	return (0);
}

// *****************************************************************************
int32_t RP_MF_JOIN(char *SSID, unsigned char secMode, char *PSK) { // ************ JOIN
	int32_t reply;
	DEBUGOUT1("MF_JOIN called...\n");

	// Frame Body Payload				bytes		#of bytes		# total number bytes
//  UINT8  uNwType					 0				1
//	UINT8  uSecType					 1				1
//	UINT8  uDataRate				 2				1
//	UINT8  uPowerValue				 3				1
//	UINT8  uPSK[32]					 4..35			32
//	UINT8  uSSID[32]				 36..67			32
//	UINT8  uIbss_mode				 68				1
//	UINT8  uChannel					 69				1 			--> 70 --> 72

	memset(RPSPI_TransmitBuffer, 0, 72); // clear SPIRP_TransmitBuffer (Payload)
	RPSPI_TransmitBuffer[0] = 1;	// --> Network Mode, 1:Infrastructure
	RPSPI_TransmitBuffer[1] = secMode;// --> Security Mode, 0:Open, 1:WPA1, 2:WPA2, 3:WEP
	RPSPI_TransmitBuffer[2] = 0;		// --> TxRate, 0:AutoRate
	// RPSPI_TransmitBuffer[ 2] = 5;			// xxx --> TxRate, 0:AutoRate  (5--> 6MBPS, see Prog. Ref. Manual 3.0, page 105, table 17)
	// RPSPI_TransmitBuffer[ 3] = 2;			// xxx transmit power value: 0(low), 1(medium), 2(high)  --> medium power
	RPSPI_TransmitBuffer[3] = 1;// transmit power value: 0(low), 1(medium), 2(high)  --> medium power
	strcpy((char*)RPSPI_TransmitBuffer + 4, PSK);	// PSK
	strcpy((char*)RPSPI_TransmitBuffer + 36, SSID);	// Network name
	RPSPI_TransmitBuffer[68] = 0;	// ibss mode, ununsed in infrastructure
	RPSPI_TransmitBuffer[69] = 0;// channel number ibss creator, unused in infrastructure

	reply = RPSPI_ManagementFrameWrite(0x12, RPSPI_TransmitBuffer, 72);	// command JOIN, length of payload
	if (reply < 0) {
		return (-1);
	}

	DEBUGOUT1("MF_JOIN: waiting for card ready...\n");
	while (Chip_GPIO_ReadPortBit(LPC_GPIO,INTERRUPT_PORT, INTERRUPT_PIN) == 0) {
	};
	if (RPSPI_ManagementFrameRead(RPSPI_ReceiveBuffer, (16 + 36)) < 0) {
		return (-1);
	}
	if (RPSPI_ReceiveBuffer[1] != 0x96) {
		DEBUGOUT2("----- error in reply W0[15:8], expected 0x96, received 0x%02X\n", RPSPI_ReceiveBuffer[1]);
		return (-1);
	}
	DEBUGOUT1("done!\n");
//  DEBUG_DisplayReceivedFrame(36);

	return (0);
}

// *****************************************************************************
// ** Data Frames **
// *****************************************************************************
int32_t RP_DF_TCPIP_Config(void) {	// ******************************* TCP/IP Config
	int32_t reply;
	// setup of transmit data structure happens below
	reply = RPSPI_DataFrameWrite(0x01, RPSPI_TransmitBuffer, 18);// command 0x01: "TCP/IP config", length of payload
	if (reply < 0) {
		return (-1);
	}

	DEBUGOUT1("DF_TCP_IP_CONFIG: waiting for card ready...\n");
	while (Chip_GPIO_ReadPortBit(LPC_GPIO,INTERRUPT_PORT, INTERRUPT_PIN) == 0) {
	};

	if (RPSPI_DataFrameRead(RPSPI_ReceiveBuffer, (16 + 24)) < 0) {// receive header(16) + 24 bytes data
		return (-1);
	}
	if (RPSPI_ReceiveBuffer[16] != 0x01) {
		DEBUGOUT2("----- error in reply CMD[7:0], expected 0x01, received 0x%02X\n", RPSPI_ReceiveBuffer[16]);
		return (-1);
	}

//  myCurrentIPAddress = *((int32_t *)(RPSPI_ReceiveBuffer+16+ 8));	  			 // this results in the wrong order of bytes :(
	myCurrentIPAddress = ULONG_IP_ADDRESS(RPSPI_ReceiveBuffer[16 + 8],
			RPSPI_ReceiveBuffer[16 + 9], RPSPI_ReceiveBuffer[16 + 10],
			RPSPI_ReceiveBuffer[16 + 11]);

	DEBUGOUT1("done!\n");
//  DEBUG_DisplayReceivedFrame(24);
	myMACAddress[0] = RPSPI_ReceiveBuffer[16 + 2];
	myMACAddress[1] = RPSPI_ReceiveBuffer[16 + 3];
	myMACAddress[2] = RPSPI_ReceiveBuffer[16 + 4];
	myMACAddress[3] = RPSPI_ReceiveBuffer[16 + 5];
	myMACAddress[4] = RPSPI_ReceiveBuffer[16 + 6];
	myMACAddress[5] = RPSPI_ReceiveBuffer[16 + 7];

	DEBUGOUT7("MAC-Address: %02X:%02X:%02X:%02X:%02X:%02X\n", RPSPI_ReceiveBuffer[16+ 2], RPSPI_ReceiveBuffer[16+ 3], RPSPI_ReceiveBuffer[16+ 4],RPSPI_ReceiveBuffer[16+ 5], RPSPI_ReceiveBuffer[16+ 6], RPSPI_ReceiveBuffer[16+ 7]);
	DEBUGOUT5("IP-Address:  %3d.%3d.%3d.%3d\n", 				  RPSPI_ReceiveBuffer[16+ 8], RPSPI_ReceiveBuffer[16+ 9], RPSPI_ReceiveBuffer[16+10], RPSPI_ReceiveBuffer[16+11]);
	DEBUGOUT5("NetMak:      %3d.%3d.%3d.%3d\n", 				  RPSPI_ReceiveBuffer[16+12], RPSPI_ReceiveBuffer[16+13], RPSPI_ReceiveBuffer[16+14], RPSPI_ReceiveBuffer[16+15]);
	DEBUGOUT5("DefGateway:  %3d.%3d.%3d.%3d\n", 				  RPSPI_ReceiveBuffer[16+16], RPSPI_ReceiveBuffer[16+17], RPSPI_ReceiveBuffer[16+18], RPSPI_ReceiveBuffer[16+19]);
	DEBUGOUT2("ErrorCode:   %ld\n", *((int32_t *) (((char *)RPSPI_ReceiveBuffer)+16+20)));

	return (0);
}

int32_t RP_DF_TCPIP_DHCP_Config(void) {
	DEBUGOUT1("DF_TCPIP_DHCP_CONFIG called...\n");

	// Frame Body Payload				bytes		#of bytes		# total number bytes
//  UINT8  uUseDHCP					 0				1
//	UINT8  aIPAddr[4]				 1..4			4
//	UINT8  aNetMask[4]			 	 5..8			4
//	UINT8  aDefGateWay[4]			 9..12			4
//	UINT8  aDNSAddr[4]				 13..16			4 			--> 17 --> 18(+2)

	memset(RPSPI_TransmitBuffer, 0, 18);// clear SPIRP_TransmitBuffer (Payload)
	RPSPI_TransmitBuffer[0] = 0x01;	   	// use DHCP enabled

	return (RP_DF_TCPIP_Config());
}

int32_t RP_DF_TCPIP_FIXIP_Config(uint32_t myIPAddress, uint32_t NetMask,
		uint32_t DefGateWay, uint32_t NameServer) { // ******************************* TCP/IP Config
	DEBUGOUT1("DF_TCPIP_FIXIP_CONFIG called...\n");

	// Frame Body Payload				bytes		#of bytes		# total number bytes
//  UINT8  uUseDHCP					 0				1
//	UINT8  aIPAddr[4]				 1..4			4
//	UINT8  aNetMask[4]			 	 5..8			4
//	UINT8  aDefGateWay[4]			 9..12			4
//	UINT8  aDNSAddr[4]				 13..16			4 			--> 17 --> 18(+2)

	memset(RPSPI_TransmitBuffer, 0, 18);// clear SPIRP_TransmitBuffer (Payload)
	RPSPI_TransmitBuffer[0] = 0x00;	   	// use DHCP disabled

	RPSPI_TransmitBuffer[1] = (myIPAddress >> 24) & 0xFF;
	RPSPI_TransmitBuffer[2] = (myIPAddress >> 16) & 0xFF;
	RPSPI_TransmitBuffer[3] = (myIPAddress >> 8) & 0xFF;
	RPSPI_TransmitBuffer[4] = (myIPAddress) & 0xFF;

	RPSPI_TransmitBuffer[5] = (NetMask >> 24) & 0xFF;
	RPSPI_TransmitBuffer[6] = (NetMask >> 16) & 0xFF;
	RPSPI_TransmitBuffer[7] = (NetMask >> 8) & 0xFF;
	RPSPI_TransmitBuffer[8] = (NetMask) & 0xFF;

	RPSPI_TransmitBuffer[9] = (DefGateWay >> 24) & 0xFF;
	RPSPI_TransmitBuffer[10] = (DefGateWay >> 16) & 0xFF;
	RPSPI_TransmitBuffer[11] = (DefGateWay >> 8) & 0xFF;
	RPSPI_TransmitBuffer[12] = (DefGateWay) & 0xFF;

	RPSPI_TransmitBuffer[13] = (NameServer >> 24) & 0xFF;
	RPSPI_TransmitBuffer[14] = (NameServer >> 16) & 0xFF;
	RPSPI_TransmitBuffer[15] = (NameServer >> 8) & 0xFF;
	RPSPI_TransmitBuffer[16] = (NameServer) & 0xFF;

	return (RP_DF_TCPIP_Config());
}

int32_t RP_DF_TCPIP_FIXIPsimple_Config(uint32_t myIPAddress) {
	uint32_t dgw;
	dgw = (myIPAddress & 0xFFFFFF00) | 1;

	return (RP_DF_TCPIP_FIXIP_Config(myIPAddress,
			ULONG_IP_ADDRESS(255, 255, 255, 0), dgw,
			ULONG_IP_ADDRESS(0, 0, 0, 0)));
}

// *****************************************************************************
int32_t RP_DF_OPEN_TCP_LISTENER(unsigned short portNumber) { // ************************ Open TCP Listener Socket (Module is server)
	int32_t reply, socketHandle;
	DEBUGOUT1("DF_TCP_SOCKET_OPEN called...\n");
	// Frame Body Payload				bytes		#of bytes		# total number bytes
//  UINT16 uSocketType				 0..1			2
//	UINT16 stLocalPort				 2..3			2
//	UINT16 rem->sInPort				 4..5			2
//	UINT8  rem->sInAddr->Addr[4]	 6..9			4 			--> 10 --> 10(+2)

	memset(RPSPI_TransmitBuffer, 0, 10);// clear SPIRP_TransmitBuffer (Payload)
	RPSPI_TransmitBuffer[0] = 0x02;	// 2: TCP Listener(Server);   (0:TCP Client, 1:UDP Client, 3:Multicast, 4:UDP Listening)
	RPSPI_TransmitBuffer[2] = (portNumber & 0xFF);	   // Listening Port lsb
	RPSPI_TransmitBuffer[3] = ((portNumber & 0xFF00) >> 8);	// Listening Port msb
	// rest unused in listener command

	reply = RPSPI_DataFrameWrite(0x02, RPSPI_TransmitBuffer, 10);// command 0x02: "Open Socket", length of payload
	if (reply < 0) {
		return (-1);
	}

	DEBUGOUT1("DF_TCP_SOCKET_OPEN: waiting for card ready...\n");
	while (Chip_GPIO_ReadPortBit(LPC_GPIO,INTERRUPT_PORT, INTERRUPT_PIN) == 0) {
	};
	if (RPSPI_DataFrameRead(RPSPI_ReceiveBuffer, (16 + 20)) < 0) {// receive header(16) + 18 bytes data
		return (-1);
	}
	if (RPSPI_ReceiveBuffer[16] != 0x02) {
		DEBUGOUT2("----- error in reply CMD[7:0], expected 0x02, received 0x%02X\n", RPSPI_ReceiveBuffer[16]);
		return (-1);
	}
	DEBUGOUT1("done!\n");
//  DEBUG_DisplayReceivedFrame(16);

	socketHandle =
			*((unsigned short *) (((char *) RPSPI_ReceiveBuffer) + 16 + 4));

	DEBUGOUT2("Socket Type:    %d\n", *((unsigned short *) (((char *)RPSPI_ReceiveBuffer)+16+2)));
//  DEBUGOUT2("Socket Handle:  %d\n", *((unsigned short *) (((char *)RPSPI_ReceiveBuffer)+16+4)));
	DEBUGOUT2("Socket Handle:  %d\n", socketHandle);
	DEBUGOUT2("Socket Port     %d\n", *((unsigned short *) (((char *)RPSPI_ReceiveBuffer)+16+6)));
	DEBUGOUT5("IP-Address:     %3d.%3d.%3d.%3d\n", RPSPI_ReceiveBuffer[16+ 8], RPSPI_ReceiveBuffer[16+ 9], RPSPI_ReceiveBuffer[16+10], RPSPI_ReceiveBuffer[16+11]);
	DEBUGOUT2("ErrorCode:      %ld\n", *((int32_t *) (((char *)RPSPI_ReceiveBuffer)+16+12)));
	DEBUGOUT2("MaxSegmentSize: %ld\n", *((unsigned short *) (((char *)RPSPI_ReceiveBuffer)+16+16)));

	return (socketHandle);
}

// *****************************************************************************
int32_t RP_DF_OPEN_TCP_CLIENT(unsigned short localPortNumber,
		unsigned short remotePortNumber, uint32_t remoteIPAddress) {
	// ************************ Open TCP Client Socket (Module is client)
	int32_t reply, socketHandle, errorCode;
	DEBUGOUT1("DF_TCP_SOCKET_CLIENT_OPEN called...\n");
	// Frame Body Payload				bytes		#of bytes		# total number bytes
//  UINT16 uSocketType				 0..1			2
//	UINT16 stLocalPort				 2..3			2
//	UINT16 rem->sInPort				 4..5			2
//	UINT8  rem->sInAddr->Addr[4]	 6..9			4 			--> 10 --> 10(+2)

	memset(RPSPI_TransmitBuffer, 0, 10);// clear SPIRP_TransmitBuffer (Payload)
	RPSPI_TransmitBuffer[0] = 0x00;	// 0:TCP Client;    (1:UDP Client, 2: TCP Listener(Server), 3:Multicast, 4:UDP Listening)
	RPSPI_TransmitBuffer[2] = (localPortNumber & 0xFF);	   // local Port lsb
	RPSPI_TransmitBuffer[3] = ((localPortNumber & 0xFF00) >> 8);// local Port msb
	RPSPI_TransmitBuffer[4] = (remotePortNumber & 0xFF);	  // remote Port lsb
	RPSPI_TransmitBuffer[5] = ((remotePortNumber & 0xFF00) >> 8);// remote Port msb
	RPSPI_TransmitBuffer[6] = ((remoteIPAddress & 0xFF000000) >> 24);// remote IP 1st byte
	RPSPI_TransmitBuffer[7] = ((remoteIPAddress & 0x00FF0000) >> 16);// remote IP 2nd byte
	RPSPI_TransmitBuffer[8] = ((remoteIPAddress & 0x0000FF00) >> 8);// remote IP 3rd byte
	RPSPI_TransmitBuffer[9] = ((remoteIPAddress & 0x000000FF));	// remote IP 4th byte

	reply = RPSPI_DataFrameWrite(0x02, RPSPI_TransmitBuffer, 10);// command 0x02: "Open Socket", length of payload
	if (reply < 0) {
		return (-1);
	}

	DEBUGOUT1("DF_TCP_SOCKET_OPEN: waiting for card ready...\n");
	while (Chip_GPIO_ReadPortBit(LPC_GPIO,INTERRUPT_PORT, INTERRUPT_PIN) == 0) {
	};
	if (RPSPI_DataFrameRead(RPSPI_ReceiveBuffer, (16 + 20)) < 0) {// receive header(16) + 18 bytes data
		return (-1);
	}
	if (RPSPI_ReceiveBuffer[16] != 0x02) {
		DEBUGOUT2("----- error in reply CMD[7:0], expected 0x02, received 0x%02X\n", RPSPI_ReceiveBuffer[16]);
		return (-1);
	}
	DEBUGOUT1("done!\n");
//  DEBUG_DisplayReceivedFrame(16);

	socketHandle =
			*((unsigned short *) (((char *) RPSPI_ReceiveBuffer) + 16 + 4));
	errorCode = *((int32_t *) (((char *) RPSPI_ReceiveBuffer) + 16 + 12));

	DEBUGOUT2("Socket Type:    %d\n", *((unsigned short *) (((char *)RPSPI_ReceiveBuffer)+16+2)));
//  DEBUGOUT2("Socket Handle:  %d\n", *((unsigned short *) (((char *)RPSPI_ReceiveBuffer)+16+4)));
	DEBUGOUT2("Socket Handle:  %d\n", socketHandle);
	DEBUGOUT2("Socket Port     %d\n", *((unsigned short *) (((char *)RPSPI_ReceiveBuffer)+16+6)));
	DEBUGOUT5("IP-Address:     %3d.%3d.%3d.%3d\n", RPSPI_ReceiveBuffer[16+ 8], RPSPI_ReceiveBuffer[16+ 9], RPSPI_ReceiveBuffer[16+10], RPSPI_ReceiveBuffer[16+11]);
	DEBUGOUT2("ErrorCode:      %ld\n", errorCode);
	DEBUGOUT2("MaxSegmentSize: %ld\n", *((unsigned short *) (((char *)RPSPI_ReceiveBuffer)+16+16)));

	if (errorCode != 0)
		return (errorCode);
	return (socketHandle);
}

// *****************************************************************************
int32_t RP_DF_OPEN_UDP_LISTENER(unsigned short portNumber) { // ************************ Open UDP Listener Socket (WLAN is server)
	int32_t reply, socketHandle, errorCode;
	DEBUGOUT1("DF_UDP_SOCKET_OPEN called...\n");
	// Frame Body Payload				bytes		#of bytes		# total number bytes
//  UINT16 uSocketType				 0..1			2
//	UINT16 stLocalPort				 2..3			2
//	UINT16 rem->sInPort				 4..5			2
//	UINT8  rem->sInAddr->Addr[4]	 6..9			4 			--> 10 --> 10(+2)

	memset(RPSPI_TransmitBuffer, 0, 10);// clear SPIRP_TransmitBuffer (Payload)
	RPSPI_TransmitBuffer[0] = 0x04;	// 2: UDP Listener(Server);   (0:TCP Client, 1:UDP Client, 2: TCP Listening, 3:Multicast)
	RPSPI_TransmitBuffer[2] = (portNumber & 0xFF);	   // Listening Port lsb
	RPSPI_TransmitBuffer[3] = ((portNumber & 0xFF00) >> 8);	// Listening Port msb
	// rest unused in listener command

	reply = RPSPI_DataFrameWrite(0x02, RPSPI_TransmitBuffer, 10);// command 0x02: "Open Socket", length of payload
	if (reply < 0) {
		return (-1);
	}

	DEBUGOUT1("DF_UDP_SOCKET_OPEN: waiting for card ready...\n");
	while (Chip_GPIO_ReadPortBit(LPC_GPIO,INTERRUPT_PORT, INTERRUPT_PIN) == 0) {
	};

	if (RPSPI_DataFrameRead(RPSPI_ReceiveBuffer, (16 + 20)) < 0) {// receive header(16) + 18 bytes data
		return (-1);
	}
	if (RPSPI_ReceiveBuffer[16] != 0x02) {
		DEBUGOUT2("----- error in reply CMD[7:0], expected 0x02, received 0x%02X\n", RPSPI_ReceiveBuffer[16]);
		return (-1);
	}
	DEBUGOUT1("done!\n");
//  DEBUG_DisplayReceivedFrame(16);

	socketHandle =
			*((unsigned short *) (((char *) RPSPI_ReceiveBuffer) + 16 + 4));
	errorCode = *((int32_t *) (((char *) RPSPI_ReceiveBuffer) + 16 + 12));

	DEBUGOUT2("Socket Type:    %d\n", *((unsigned short *) (((char *)RPSPI_ReceiveBuffer)+16+2)));
//  DEBUGOUT2("Socket Handle:  %d\n", *((unsigned short *) (((char *)RPSPI_ReceiveBuffer)+16+4)));
	DEBUGOUT2("Socket Handle:  %d\n", socketHandle);
	DEBUGOUT2("Socket Port     %d\n", *((unsigned short *) (((char *)RPSPI_ReceiveBuffer)+16+6)));
	DEBUGOUT5("IP-Address:     %3d.%3d.%3d.%3d\n", RPSPI_ReceiveBuffer[16+ 8], RPSPI_ReceiveBuffer[16+ 9], RPSPI_ReceiveBuffer[16+10], RPSPI_ReceiveBuffer[16+11]);
	DEBUGOUT2("ErrorCode:      %ld\n", errorCode);
	DEBUGOUT2("MaxSegmentSize: %ld\n", *((unsigned short *) (((char *)RPSPI_ReceiveBuffer)+16+16)));

	if (errorCode != 0)
		return (errorCode);

	return (socketHandle);
}

// *****************************************************************************
int32_t RP_DF_OPEN_UDP_CLIENT(unsigned short localPortNumber,
		unsigned short remotePortNumber, uint32_t remoteIPAddress) {
	// ************************ Open UDP Listener Socket (WLAN is server)
	int32_t reply, socketHandle;
	DEBUGOUT1("DF_UDP_SOCKET_OPEN called...\n");
	// Frame Body Payload				bytes		#of bytes		# total number bytes
//  UINT16 uSocketType				 0..1			2
//	UINT16 stLocalPort				 2..3			2
//	UINT16 rem->sInPort				 4..5			2
//	UINT8  rem->sInAddr->Addr[4]	 6..9			4 			--> 10 --> 10(+2)

	memset(RPSPI_TransmitBuffer, 0, 10);// clear SPIRP_TransmitBuffer (Payload)
	RPSPI_TransmitBuffer[0] = 0x01;	// 1:UDP Client;    (0:TCP Client, 2: TCP Listener(Server), 3:Multicast, 4:UDP Listening)
	RPSPI_TransmitBuffer[2] = (localPortNumber & 0xFF);	   // local Port lsb
	RPSPI_TransmitBuffer[3] = ((localPortNumber & 0xFF00) >> 8);// local Port msb
	RPSPI_TransmitBuffer[4] = (remotePortNumber & 0xFF);	  // remote Port lsb
	RPSPI_TransmitBuffer[5] = ((remotePortNumber & 0xFF00) >> 8);// remote Port msb
	RPSPI_TransmitBuffer[6] = ((remoteIPAddress & 0xFF000000) >> 24);// remote IP 1st byte
	RPSPI_TransmitBuffer[7] = ((remoteIPAddress & 0x00FF0000) >> 16);// remote IP 2nd byte
	RPSPI_TransmitBuffer[8] = ((remoteIPAddress & 0x0000FF00) >> 8);// remote IP 3rd byte
	RPSPI_TransmitBuffer[9] = ((remoteIPAddress & 0x000000FF));	// remote IP 4th byte

	reply = RPSPI_DataFrameWrite(0x02, RPSPI_TransmitBuffer, 10);// command 0x02: "Open Socket", length of payload
	if (reply < 0) {
		return (-1);
	}

	DEBUGOUT1("DF_UDP_SOCKET_OPEN: waiting for card ready...\n");
	while (Chip_GPIO_ReadPortBit(LPC_GPIO,INTERRUPT_PORT, INTERRUPT_PIN) == 0) {
	};

	if (RPSPI_DataFrameRead(RPSPI_ReceiveBuffer, (16 + 20)) < 0) {// receive header(16) + 18 bytes data
		return (-1);
	}
	if (RPSPI_ReceiveBuffer[16] != 0x02) {
		DEBUGOUT2("----- error in reply CMD[7:0], expected 0x02, received 0x%02X\n", RPSPI_ReceiveBuffer[16]);
		return (-1);
	}
	DEBUGOUT1("done!\n");
//  DEBUG_DisplayReceivedFrame(16);

	socketHandle =
			*((unsigned short *) (((char *) RPSPI_ReceiveBuffer) + 16 + 4));

	DEBUGOUT2("Socket Type:    %d\n", *((unsigned short *) (((char *)RPSPI_ReceiveBuffer)+16+2)));
//  DEBUGOUT2("Socket Handle:  %d\n", *((unsigned short *) (((char *)RPSPI_ReceiveBuffer)+16+4)));
	DEBUGOUT2("Socket Handle:  %d\n", socketHandle);
	DEBUGOUT2("Socket Port     %d\n", *((unsigned short *) (((char *)RPSPI_ReceiveBuffer)+16+6)));
	DEBUGOUT5("IP-Address:     %3d.%3d.%3d.%3d\n", RPSPI_ReceiveBuffer[16+ 8], RPSPI_ReceiveBuffer[16+ 9], RPSPI_ReceiveBuffer[16+10], RPSPI_ReceiveBuffer[16+11]);
	DEBUGOUT2("ErrorCode:      %ld\n", *((int32_t *) (((char *)RPSPI_ReceiveBuffer)+16+12)));
	DEBUGOUT2("MaxSegmentSize: %ld\n", *((unsigned short *) (((char *)RPSPI_ReceiveBuffer)+16+16)));

	return (socketHandle);
}

// *****************************************************************************
int32_t RP_DF_CLOSE_SOCKET(unsigned short socketNumber) { // ********** Close Socket
	int32_t reply;
	DEBUGOUT1("DF_CLOSE_SOCKET called...\n");
	// Frame Body Payload				bytes		#of bytes		# total number bytes
//  UINT16 uSocketPort				 0..1			2 			--> 2 --> 2(+2)

	memset(RPSPI_TransmitBuffer, 0, 2);	// clear SPIRP_TransmitBuffer (Payload)
	RPSPI_TransmitBuffer[0] = (socketNumber & 0xFF);	// Socket Number lsb
	RPSPI_TransmitBuffer[3] = ((socketNumber & 0xFF00) >> 8);// Socket Number msb

	reply = RPSPI_DataFrameWrite(0x04, RPSPI_TransmitBuffer, 2);// command 0x04: "Close Socket", length of payload
	if (reply < 0) {
		return (-1);
	}

	DEBUGOUT1("DF_CLOSE_SOCKET: waiting for card ready...\n");
	while (Chip_GPIO_ReadPortBit(LPC_GPIO,INTERRUPT_PORT, INTERRUPT_PIN) == 0) {
	};

	if (RPSPI_DataFrameRead(RPSPI_ReceiveBuffer, (16 + 8)) < 0) {// receive header(16) + 8 bytes data
		return (-1);
	}
	if (RPSPI_ReceiveBuffer[16] != 0x06) {
		DEBUGOUT2("----- error in reply CMD[7:0], expected 0x06, received 0x%02X\n", RPSPI_ReceiveBuffer[16]);
		return (-1);
	}
	DEBUGOUT1("done!\n");
//  DEBUG_DisplayReceivedFrame(16);

	DEBUGOUT2("Socket Port     %d\n", *((unsigned short *) (((char *)RPSPI_ReceiveBuffer)+16+2)));
	DEBUGOUT2("ErrorCode:      %ld\n", *((int32_t *) (((char *)RPSPI_ReceiveBuffer)+16+4)));

	return (0);
}

// *****************************************************************************
int32_t RP_DF_QUERY_FIRMWARE_VERSION(char *fwString) { // ********************* Query Firmware Version
	int32_t reply;
	DEBUGOUT1("DF_QFW called...\n");
	// Frame Body Payload				bytes		#of bytes		# total number bytes
	// none (empty)								 			  --> 0 --> 0 (+2)

	memset(RPSPI_TransmitBuffer, 0, 2);	// clear SPIRP_TransmitBuffer (Payload)
	reply = RPSPI_DataFrameWrite(0x0D, RPSPI_TransmitBuffer, 2);// command 0x0D: Query Firmware
	if (reply < 0) {
		return (-1);
	}

	DEBUGOUT1("DF_QFW: waiting for card ready...\n");
	while (Chip_GPIO_ReadPortBit(LPC_GPIO,INTERRUPT_PORT, INTERRUPT_PIN) == 0) {
	};

	if (RPSPI_DataFrameRead(RPSPI_ReceiveBuffer, (16 + 24)) < 0) {// receive header(16) + 20(+2) --> 24 bytes data
		return (-1);
	}
	if (RPSPI_ReceiveBuffer[16] != 0x0F) {
		DEBUGOUT2("----- error in reply CMD[7:0], expected 0x0F, received 0x%02X\n", RPSPI_ReceiveBuffer[16]);
		return (-1);
	}
	DEBUGOUT1("done!\n");
//  DEBUG_DisplayReceivedFrame(20);

	DEBUGOUT2("Firmware Version: %s\n", (RPSPI_ReceiveBuffer+(16+2)));
	strcpy(fwString, (char*)(RPSPI_ReceiveBuffer + (16 + 2)));

	return (0);
}

// *****************************************************************************
// *****************************************************************************
int32_t xxUNUSEDxxRPTransmitBufferFree(void) {
	if ((RPSPI_SPIRegisterRead(0x00) & 0x01) == 0) {
		return (1);
	}
	return (0);
}

// *****************************************************************************
int32_t RPSendTCPDataPacket(uint32_t socketNumber, char *data,
		int32_t dataLength) {

	// Frame Body Payload				bytes		#of bytes		# total number bytes
//  UINT16 uSocketDescriptor		 0..1			2
//	UINT32 uBufferLength			 2..5			4
//	UINT16 uDataOffset				 6..7			2
//	UINT8  aDataBuffer[1400]		 8..xxx			 			--> 8+xx --> 10(+2)

	unsigned char dataPacket[2 + 4 + 2 + 1400];
	int32_t reply, n, packetLength;

	reply = RPSPI_SPIRegisterRead(0x00);
	if (reply & 0x01) {		 				// buffer empty?
		DEBUGOUT1("RPSendTCPDataPacket: Transmit Buffer full!\n");

#ifdef WLAN_TCP_DELAY_DEBUG
		if (reply == 0x01) {
			putint8_tUART1('.');		// indicate buffer really full
		} else {
			putint8_tUART1('E');		// indicate something else happened
		}
#endif	

		return (-1);
	}

	packetLength = 0 + 2 + 4 + 2 + 44 + dataLength;	// compute total data length:
													//   (2 -- command type, mandatory for data frames --- will get added in send_data_frame)
													//    2 -- socket descriptor
													//    4 -- uBufferLength (which will be ignored!)
													//    2 -- uDataOffset
													//   44 - dataOffset
DEBUGOUT2("RPSendTCPDataPacket: dataLength=%d\n", dataLength);
DEBUGOUT2("RPSendTCPDataPacket: packetLength=%d\n", packetLength);

	dataPacket[0] = (socketNumber & 0xFF);			// copy socket number
	dataPacket[1] = ((socketNumber >> 8) & 0xFF);

	// according to RedPine this value is ignored! It should report the number of bytes to be transmitted.
	// my investigation confirms that this number is ignored when sending!
	dataPacket[2] = 0; // ((dataLength)     & 0xFF);		// fill in dataLength
	dataPacket[3] = 0;  // ((dataLength>> 8) & 0xFF);
	dataPacket[4] = 0;  // ((dataLength>>16) & 0xFF);
	dataPacket[5] = 0;  // ((dataLength>>24) & 0xFF);
//  dataPacket[2] = ((dataLength)     & 0xFF);		// fill in dataLength
//  dataPacket[3] = ((dataLength>> 8) & 0xFF);
//  dataPacket[4] = ((dataLength>>16) & 0xFF);
//  dataPacket[5] = ((dataLength>>24) & 0xFF);

	dataPacket[6] = (2 + 2 + 4 + 2 + 44); // uDataOffset TCP (here we need to add-in the first 2 "command" bytes as well)
	dataPacket[7] = 0;

	for (n = 8; n < (8 + 44); n++) {				// clear offset buffer
		dataPacket[n] = 0;
	}

	while (dataLength) {	   							// copy data
		dataPacket[n] = *data;
		n++;
		data++;
		dataLength--;
	}
	// done filling the buffer, call send!

	reply = RPSPI_DataFrameWrite(0x03, dataPacket, packetLength);// command 0x03: Send data
	if (reply < 0) {
		return (-1);
	}

	return (0);
}

// *****************************************************************************
int32_t RPSendUDPDataPacket(uint32_t socketNumber,
		uint32_t destIPAddress, uint32_t destPort, char *data,
		int32_t dataLength) {

	// Frame Body Payload				bytes		#of bytes		# total number bytes
//  UINT16 uSocketDescriptor		 0..1			2
//	UINT32 uBufferLength			 2..5			4
//	UINT16 uDataOffset				 6..7			2
//	UINT8  aDataBuffer[1400]		 8..xxx			 			--> 8+xx --> 10(+2)
//    UINT16 DestinationPort         8..9           2
//    UINT32 DestinationIP			 10..13			4
//    UINT8  offset+Payload

	unsigned char dataPacket[2 + 4 + 2 + 1400];
	int32_t reply, n, packetLength;

	reply = RPSPI_SPIRegisterRead(0x00);
	if (reply & 0x01) {		 				// buffer empty?
		DEBUGOUT1("RPSendUDPDataPacket: Transmit Buffer full!\n");

#ifdef WLAN_TCP_DELAY_DEBUG
		if (reply == 0x01) {
			putint8_tUART1(',');		// indicate buffer really full
		} else {
			putint8_tUART1('A');		// indicate something else happened
		}
#endif
		return (-1);
	}

	packetLength = 0 + 2 + 4 + 2 + 2 + 4 + 26 + dataLength;	// compute total data length:
															//   (2 -- command type, mandatory for data frames --- will get added in send_data_frame)
															//    2 -- socket descriptor
															//    4 -- uBufferLength (which will be ignored!)
															//    2 -- uDataOffset
															//    2 -- DestinationPort
															//    4 -- DestinationIP
															//   26 - dataOffset
DEBUGOUT2("RPSendUDPDataPacket: dataLength=%d\n", dataLength);
DEBUGOUT2("RPSendUDPDataPacket: packetLength=%d\n", packetLength);

	dataPacket[0] = (socketNumber & 0xFF);			// copy socket number
	dataPacket[1] = ((socketNumber >> 8) & 0xFF);

	// according to RedPine this value is ignored! It should report the number of bytes to be transmitted.
	// my investigation confirms that this number is ignored when sending!
	dataPacket[2] = 0; // ((dataLength)     & 0xFF);		// fill in dataLength
	dataPacket[3] = 0;  // ((dataLength>> 8) & 0xFF);
	dataPacket[4] = 0;  // ((dataLength>>16) & 0xFF);
	dataPacket[5] = 0;  // ((dataLength>>24) & 0xFF);
//  dataPacket[2] = ((dataLength)     & 0xFF);		// fill in dataLength
//  dataPacket[3] = ((dataLength>> 8) & 0xFF);
//  dataPacket[4] = ((dataLength>>16) & 0xFF);
//  dataPacket[5] = ((dataLength>>24) & 0xFF);

	dataPacket[6] = (2 + 2 + 4 + 2 + 2 + 4 + 26); // uDataOffset TCP (here we need to add-in the first 2 "command" bytes as well)
	dataPacket[7] = 0;

DEBUGOUT2("RPSendUDPDataPacket: DestinationPort=%d\n", destPort);
	dataPacket[8] = (destPort & 0xFF);	   			// Destination Port lsb
	dataPacket[9] = ((destPort & 0xFF00) >> 8);	   	// Destination Port msb

DEBUGOUT2("RPSendUDPDataPacket: DestinationIP=%d\n", destIPAddress);
DEBUGOUT5("RPSendUDPDataPacket: DestinationIP=%d.%d.%d.%d\n", (destIPAddress&0xFF000000)>>24, (destIPAddress&0x00FF0000)>>16, (destIPAddress&0x0000FF00)>>8, (destIPAddress&0x000000FF));
	dataPacket[10] = (destIPAddress >> 24) & 0xFF;
	dataPacket[11] = (destIPAddress >> 16) & 0xFF;
	dataPacket[12] = (destIPAddress >> 8) & 0xFF;
	dataPacket[13] = (destIPAddress) & 0xFF;

	for (n = 14; n < (14 + 26); n++) {				// clear offset buffer
		dataPacket[n] = 0;
	}

	while (dataLength) {	   							// copy data
		dataPacket[n] = *data;
		n++;
		data++;
		dataLength--;
	}
	// done filling the buffer, call send!

	reply = RPSPI_DataFrameWrite(0x03, dataPacket, packetLength);// command 0x03: Send data
	if (reply < 0) {
		return (-1);
	}

	return (0);
}

// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
// ** RP Load Boot Image
// *****************************************************************************
int32_t RPLoadBootImages(void) {
	uint32_t d;

	DEBUGOUT1("RPLBI: writing 0x00000001 to adr 0x22000004...\n");
	d = 0x00000001;
	(void) RPSPI_MemoryWrite(0x22000004, (unsigned char *) &d, 4);
	timerDelayMs(10);

	DEBUGOUT1("RPLBI: loading sbinst1 to adr 0x00000000...\n");
	(void) RPSPI_MemoryWrite(0x00000000, (unsigned char *) sbinst1, sizeof(sbinst1));
	timerDelayMs(10);

	DEBUGOUT1("RPLBI: loading sbinst2 to adr 0x02014010...\n");
	(void) RPSPI_MemoryWrite(0x02014010, (unsigned char *) sbinst2, sizeof(sbinst2));
	timerDelayMs(10);

	DEBUGOUT1("RPLBI: loading sbdata1 to adr 0x20000000...\n");
	(void) RPSPI_MemoryWrite(0x20000000, (unsigned char *) sbdata1, sizeof(sbdata1));
//  DEBUGOUT1 ("spiRP: loading sbdata1 to adr 0x20003100...\n");
//  (void) SPI_MemoryWrite(0x20003100, (char *) sbdata1, sizeof(sbdata1));
	timerDelayMs(10);

	DEBUGOUT1("RPLBI: writing 0x00000000 to adr 0x22000004...\n");
	d = 0x00000000;
	(void) RPSPI_MemoryWrite(0x22000004, (unsigned char *) &d, 4);
	timerDelayMs(10);

	DEBUGOUT1("RPLBI: waiting for card ready...\n");
	while (Chip_GPIO_ReadPortBit(LPC_GPIO,INTERRUPT_PORT, INTERRUPT_PIN) == 0) {
	};
	(void) RPSPI_ManagementFrameRead(RPSPI_ReceiveBuffer, 16);

	if (RPSPI_ReceiveBuffer[1] != 0x93) {
		DEBUGOUT2("----- error in reply W0[15:8], expected 0x93, received 0x%02X   --- INGORED\n", RPSPI_ReceiveBuffer[1]);
//	return(-1);
	}
	timerDelayMs(10);

	DEBUGOUT1("RPLBI: loading sbdata2 to adr 0x02010000...\n");
	(void) RPSPI_MemoryWrite(0x02010000, (unsigned char *) sbdata2, sizeof(sbdata2));
	timerDelayMs(10);

	DEBUGOUT1("RPLBI: done\n");
	return (0);
}
