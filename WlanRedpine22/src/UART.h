/*
 * UART.h
 *
 *  Created on: Jun 10, 2014
 *      Author: raraujo
 */

#ifndef UART_H_
#define UART_H_

#include <stdint.h>


#define LEDSetOn() LEDSetState(1)
#define LEDSetOff() LEDSetState(0)
#define LEDSetBlinking() LEDSetState(((long) -1))
#define LEDSetTime(x) LEDSetState(x)

// *****************************************************************************
#define TCP_PORT_OFFSET						((uint16_t) (+0))
//#define UDP_PORT_OFFSET						((uint16_t) (+2))
#define UDP_PORT_OFFSET						((uint16_t) (myCurrentIPAddress&0xFF))	// use individual UDP address for each active module!
#define TCP_REPROG_PORT_OFFSET				((uint16_t) (+4))
#define UART1_PORT_OFFSET					((uint16_t) (+1))
#define CTRL_PORT_TCP_OFFSET				((uint16_t) (+6))
#define CTRL_PORT_UDP_OFFSET				((uint16_t) (+7))

#define SOFTWARE_VERSION			"2.9"
#define FIRMWARE_VERSION			"4.7.[3,4] - test"
#define WLAN_WELCOME_MESSAGE		"WLAN CTRL - "__DATE__", "__TIME__" - V"SOFTWARE_VERSION"  FW"FIRMWARE_VERSION"-22(2.4GHz)\n"

#define DEFAULT_BAUDRATE					((uint32_t) 4000000)

#define REPROG_BAUDRATE 					((uint32_t)   62500)


#define UART_INBUFFER_SIZE_BITS			6
#define UART_OUTBUFFER_SIZE_BITS		12
#define UART_INBUFFER_SIZE				((uint32_t) (((uint32_t) 1)<<UART_INBUFFER_SIZE_BITS))
#define UART_INBUFFER_MASK				(UART_INBUFFER_SIZE - 1)
#define UART_OUTBUFFER_SIZE				((uint32_t) (((uint32_t) 1)<<UART_OUTBUFFER_SIZE_BITS))
#define UART_OUTBUFFER_MASK				(UART_OUTBUFFER_SIZE - 1)


extern void putcharUART1(int8_t charToSend);
extern void uart0InitiateInterruptSending(void);
extern void uart0WaitForTransferDone(void);
extern void uart1WaitForTransferDone(void);
extern void uart0SendString(unsigned char *s, uint32_t l);
extern void uart1SendString(unsigned char *s, uint32_t l);

extern void uart0SetBaudRate(uint32_t baudRate);
extern void uart1SetBaudRate(uint32_t baudRate);
extern void uart0SetRTSCTSEnable(uint32_t flag);
extern void uart1SetRTSCTSEnable(uint32_t flag);

extern int32_t parseCommand(char *c);

extern void uartInit(void);
extern void uartIterate(void);

#endif /* UART_H_ */
