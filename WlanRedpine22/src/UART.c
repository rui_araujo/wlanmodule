#include <string.h>
#include "UART.h"
#include "chip.h"
#include "RPinterface.h"
#include "RPLib.h"
#include "uart_pinout.h"
#include "rp_spi_driver.h"
#include "xprintf.h"
#include <cr_section_macros.h>

extern void resetDevice(void);
extern void enterReprogrammingMode(void);
extern void LEDSetState(long state);

// *****************************************************************************
uint32_t UARTCommandModeActive;
extern uint32_t myCurrentIPAddress;
extern uint32_t myMACAddress[];
extern uint32_t commandModeEchoActive;

uint8_t uart0OutBuffer[UART_OUTBUFFER_SIZE];	// buffer to transmit RS232 data
int32_t uart0OutBufferWritePointer;
volatile int32_t uart0OutBufferReadPointer;
uint32_t uart0RTSCTSEnableFlag;

uint32_t lastUART0TimeOutCharacter = 0;   		   // if timeout, remember last character (for reprogramming mode)

uint8_t uart1OutBuffer[UART_OUTBUFFER_SIZE];	// buffer to transmit RS232 data
int32_t uart1OutBufferWritePointer;
volatile int32_t uart1OutBufferReadPointer;
uint32_t uart1RTSCTSEnableFlag;

#ifdef DEBUG_WLAN_STRESSTEST
uint32_t DEBUG_WLAN_ENABLE = 1, debugCounter = 0;
#endif

// *****************************************************************************
char commandLine[COMMAND_LINE_MAX_LENGTH];
uint32_t commandLinePointer;
char commandReturn[RETURN_LINE_MAX_LENGTH];

// *****************************************************************************
volatile uint32_t uart0TimeoutValue;
volatile uint32_t uart1TimeoutValue;

// *****************************************************************************
extern uint32_t socketHandleTCP0, socketHandleUDP0, socketHandleTCPRP0;
extern uint32_t ipAddressMemory0;
extern uint32_t uart0ActiveSocket;

extern uint32_t socketHandleTCP1, socketHandleUDP1, socketHandleTCPRP1;
extern uint32_t ipAddressMemory1;
extern uint32_t uart1ActiveSocket;

extern uint32_t socketHandleCTRLTCP, socketHandleCTRLUDP;

// *****************************************************************************
#define TCP_PACKET_SIZE_BITS			(10)
#define TCP_PACKET_SIZE					(_BIT(TCP_PACKET_SIZE_BITS))
#define TCP_PACKET_SIZE_MASK			(TCP_PACKET_SIZE-1)
#define TCP_BUF_COUNT					(16)					// in terms of Packets, use power of 2!#define TCP_BUF_TOTAL_SIZE				(TCP_PACKET_SIZE*TCP_BUF_COUNT)#define TCP_BUF_TOTAL_SIZE_MASK			(TCP_BUF_TOTAL_SIZE-1)#define TCP_BUF_INDEX(x)  				((x) >> (TCP_PACKET_SIZE_BITS))#define TCP_BUFFER_START(x)				((x) << (TCP_PACKET_SIZE_BITS))char TCPqueue0[TCP_BUF_TOTAL_SIZE];volatile uint32_t TCPpointer0;uint32_t TCPBufferLength0[TCP_BUF_COUNT];uint32_t uart0TCPNextBlockToSend = 0;__BSS(RAM2) char TCPqueue1[TCP_BUF_TOTAL_SIZE];volatile uint32_t TCPpointer1;
uint32_t TCPBufferLength1[TCP_BUF_COUNT];
uint32_t uart1TCPNextBlockToSend = 0;

// *****************************************************************************
void UART0_IRQHandler(void) {
	uint32_t interruptIdentifier;
	uint32_t charCounter;

	// this shows what happened, read to clear flag
	while (((interruptIdentifier = LPC_UART0->IIR) & 0x01) == 0) {	// still an interrupt pending?

		switch (interruptIdentifier & 0x0E) {

		case 0x02:	// Transmit buffer empty
			if (uart0OutBufferWritePointer != uart0OutBufferReadPointer) {
				charCounter = 16;			   	  			// 16FIFO + 1 TxRegister
				do {
					LPC_UART0->THR = uart0OutBuffer[uart0OutBufferReadPointer];
					uart0OutBufferReadPointer++;
					uart0OutBufferReadPointer &= UART_OUTBUFFER_MASK;
					charCounter--;
				} while ((uart0OutBufferWritePointer != uart0OutBufferReadPointer) && (charCounter));
			}

			if ((uart0OutBufferWritePointer == uart0OutBufferReadPointer)
					|| (Chip_GPIO_ReadPortBit(LPC_GPIO, UART0_RTS_PORT, UART0_RTS_PIN) && (uart0RTSCTSEnableFlag))) {
				LPC_UART0->IER &= ~(0x02);		 	   //  disable transmission interrupt
			}

			break;

		case 0x0C:	// Receive data (timeout) available  (CTI)
		case 0x04:	// Receive data (14 chars) available (RDA)
			do {
				TCPqueue0[TCPpointer0] = LPC_UART0->RBR;
				TCPpointer0++;
				TCPpointer0 &= TCP_BUF_TOTAL_SIZE_MASK;
			} while (LPC_UART0->LSR & 0x01);				   // continue with possibly further chars

			{ 			  											// check buffer fill level (for rts/cts)
				long emptyBufferCount;
				emptyBufferCount = (uart0TCPNextBlockToSend - TCP_BUF_INDEX(TCPpointer0)) - 1;
				if (emptyBufferCount <= 0)
					emptyBufferCount += TCP_BUF_COUNT;
				if ((emptyBufferCount > 0) && (emptyBufferCount <= 4)) {
					Chip_GPIO_SetPinOutHigh(LPC_GPIO, UART0_CTS_PORT, UART0_CTS_PIN);  // set CTS pin to high -> no more data, please.
				}
				uart0TimeoutValue = 0;
			}
			break;

		default:  //unknown cause
			charCounter = LPC_UART0->LSR;		   // dummy read to clear int
//        charCounter = LPC_UART0->RBR;		   // do not read char from UART input
			break;
		}
	}

}

// *****************************************************************************
void UART2_IRQHandler(void) {
	uint32_t interruptIdentifier;
	uint32_t charCounter;

	// this shows what happened, read to clear flag
	while (((interruptIdentifier = LPC_UART2->IIR) & 0x01) == 0) {	// still an interrupt pending?

		switch (interruptIdentifier & 0x0E) {

		case 0x02:	// Transmit buffer empty
			if (uart1OutBufferWritePointer != uart1OutBufferReadPointer) {
				charCounter = 16;			   	  			// 16FIFO + 1 TxRegister
				do {
					LPC_UART2->THR = uart1OutBuffer[uart1OutBufferReadPointer];
					uart1OutBufferReadPointer++;
					uart1OutBufferReadPointer &= UART_OUTBUFFER_MASK;
					charCounter--;
				} while ((uart1OutBufferWritePointer != uart1OutBufferReadPointer) && (charCounter));
			}

			if ((uart1OutBufferWritePointer == uart1OutBufferReadPointer)
					|| (Chip_GPIO_ReadPortBit(LPC_GPIO, UART2_RTS_PORT, UART2_RTS_PIN) && (uart1RTSCTSEnableFlag))) {
				LPC_UART2->IER &= ~(0x02);		 	   //  disable transmission interrupt
			}

			break;

		case 0x0C:	// Receive data (timeout) available  (CTI)
		case 0x04:	// Receive data (14 chars) available (RDA)
			do {
				TCPqueue1[TCPpointer1] = LPC_UART2->RBR;
				TCPpointer1++;
				TCPpointer1 &= TCP_BUF_TOTAL_SIZE_MASK;
			} while (LPC_UART2->LSR & 0x01);				   // continue with possibly further chars

			{ 			  											// check buffer fill level (for rts/cts)
				signed long emptyBufferCount;
				emptyBufferCount = (uart1TCPNextBlockToSend - TCP_BUF_INDEX(TCPpointer1)) - 1;
				if (emptyBufferCount <= 0)
					emptyBufferCount += TCP_BUF_COUNT;
				if ((emptyBufferCount > 0) && (emptyBufferCount <= 4)) {
					Chip_GPIO_SetPinOutHigh(LPC_GPIO, UART2_CTS_PORT, UART2_CTS_PIN);  // set CTS pin to high -> no more data, please.
				}
				uart1TimeoutValue = 0;
			}
			break;

		default:  //unknown cause
			charCounter = LPC_UART2->LSR;		   // dummy read to clear int
//        charCounter = LPC_UART2->RBR;		   // do not read char from UART input
			break;
		}
	}

}

void putcharUART0(char charToSend) {
	LPC_UART0->IER = 0x01;		  	 									// enable rx interrupt only  -> disble tx interrupt
	uart0OutBuffer[uart0OutBufferWritePointer] = charToSend;			// store char to send in buffer
	uart0OutBufferWritePointer++;
	uart0OutBufferWritePointer &= UART_OUTBUFFER_MASK;

	if (uart0OutBufferWritePointer == uart0OutBufferReadPointer) {	// buffer full?
		uart0OutBufferReadPointer++;	   							 	// skip char at beginning of buffer? sorry!
		uart0OutBufferReadPointer &= UART_OUTBUFFER_MASK;
	}

}

// *****************************************************************************
void putcharUART1(int8_t charToSend) {
	LPC_UART2->IER = 0x01;		  	  										// enable rx interrupt only  -> disble tx interrupt
	uart1OutBuffer[uart1OutBufferWritePointer] = charToSend;		  	  										// store int8_t to send in buffer
	uart1OutBufferWritePointer++;
	uart1OutBufferWritePointer &= UART_OUTBUFFER_MASK;

	if (uart1OutBufferWritePointer == uart1OutBufferReadPointer) {	// buffer full?
		uart1OutBufferReadPointer++;	// skip int8_t at beginning of buffer? sorry!
		uart1OutBufferReadPointer &= UART_OUTBUFFER_MASK;
	}
}

// *****************************************************************************
void uart0InitiateInterruptSending(void) {
	LPC_UART0->IER = 0x01;		  	  						// enable rx interrupt only  -> disble tx interrupt

	if ((~uart0RTSCTSEnableFlag) || (Chip_GPIO_ReadPortBit(LPC_GPIO, UART0_RTS_PORT, UART0_RTS_PIN) == 0)) {		// may we send data?
		while ((LPC_UART0->LSR & _BIT(5)) && (uart0OutBufferWritePointer != uart0OutBufferReadPointer)) { // space in transmitter and data available?
			LPC_UART0->THR = uart0OutBuffer[uart0OutBufferReadPointer];
			uart0OutBufferReadPointer++;
			uart0OutBufferReadPointer &= UART_OUTBUFFER_MASK;
		}
		if (uart0OutBufferWritePointer != uart0OutBufferReadPointer) {
			LPC_UART0->IER = 0x03;			 				// enable rx+tx interrupts
		}
	}
}

// *****************************************************************************
void uart0WaitForTransferDone(void) {
	while (uart0OutBufferWritePointer != uart0OutBufferReadPointer) {
		uart0InitiateInterruptSending();
	}  // wait for buffer empty
	while ((LPC_UART0->LSR & 0x40) == 0) {
	}		// wait for characters transmitted (transmitter completely empty)
}

// *****************************************************************************
void uart1InitiateInterruptSending(void) {
	LPC_UART2->IER = 0x01;		  	  						// enable rx interrupt only  -> disble tx interrupt

	if ((~uart1RTSCTSEnableFlag) || (Chip_GPIO_ReadPortBit(LPC_GPIO, UART2_RTS_PORT, UART2_RTS_PIN) == 0)) {		// may we send data?
		while ((LPC_UART2->LSR & _BIT(5)) && (uart1OutBufferWritePointer != uart1OutBufferReadPointer)) { // space in transmitter and data available?
			LPC_UART2->THR = uart1OutBuffer[uart1OutBufferReadPointer];
			uart1OutBufferReadPointer++;
			uart1OutBufferReadPointer &= UART_OUTBUFFER_MASK;
		}
		if (uart1OutBufferWritePointer != uart1OutBufferReadPointer) {
			LPC_UART2->IER = 0x03;			 				// enable rx+tx interrupts
		}
	}
}

// *****************************************************************************
void uart1WaitForTransferDone(void) {
	while (uart1OutBufferWritePointer != uart1OutBufferReadPointer) {
		uart1InitiateInterruptSending();
	}  // wait for buffer empty
	while ((LPC_UART2->LSR & 0x40) == 0) {
	}		// wait for characters transmitted (transmitter completely empty)
}

// *****************************************************************************
void uart0SendString(unsigned char *s, uint32_t l) {
	LPC_UART0->IER = 0x01;		  	  								// enable rx interrupt only  -> disble tx interrupt

	while (l--) {
		uart0OutBuffer[uart0OutBufferWritePointer] = *s;		// store int8_t to send in buffer
		s++;
		uart0OutBufferWritePointer++;
		uart0OutBufferWritePointer &= UART_OUTBUFFER_MASK;

		if (uart0OutBufferWritePointer == uart0OutBufferReadPointer) {	// buffer full?
			uart0OutBufferReadPointer++;	   							 	// skip int8_t at beginning of buffer? sorry!
			uart0OutBufferReadPointer &= UART_OUTBUFFER_MASK;
		}
	}

	(void) uart0InitiateInterruptSending();
}

// *****************************************************************************
void uart1SendString(unsigned char *s, uint32_t l) {
	LPC_UART2->IER = 0x01;		  	  								// enable rx interrupt only  -> disble tx interrupt

	while (l--) {
		uart1OutBuffer[uart1OutBufferWritePointer] = *s;		// store int8_t to send in buffer
		s++;
		uart1OutBufferWritePointer++;
		uart1OutBufferWritePointer &= UART_OUTBUFFER_MASK;

		if (uart1OutBufferWritePointer == uart1OutBufferReadPointer) {	// buffer full?
			uart1OutBufferReadPointer++;	// skip int8_t at beginning of buffer? sorry!
			uart1OutBufferReadPointer &= UART_OUTBUFFER_MASK;
		}
	}
	(void) uart1InitiateInterruptSending();
}

// *****************************************************************************
void uart0SetRTSCTSEnable(uint32_t flag) {
	uart0RTSCTSEnableFlag = flag;
}

// *****************************************************************************
void uart1SetRTSCTSEnable(uint32_t flag) {
	uart1RTSCTSEnableFlag = flag;
}

// *****************************************************************************
void uart0SetBaudRate(uint32_t baudRate) {
	Chip_UART_SetBaudFDR(LPC_UART0, baudRate);
}

// *****************************************************************************
void uart1SetBaudRate(uint32_t baudRate) {
	Chip_UART_SetBaudFDR(LPC_UART2, baudRate);
}

// *****************************************************************************
void uartInit(void) {
	uint32_t n;
	Chip_GPIO_SetPinState(LPC_GPIO, 2, 2, 0); //Signal ready to the DTE
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, 2, 2);
	Chip_GPIO_SetPinDIRInput(LPC_GPIO, 2, 0);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, 22, 0); //Signal ready to the DTE
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, 0, 22);
	Chip_GPIO_SetPinDIRInput(LPC_GPIO, 0, 1);
	Chip_UART_Init(LPC_UART0);
	Chip_UART_ConfigData(LPC_UART0, (UART_LCR_WLEN8 | UART_LCR_SBS_1BIT));
	Chip_UART_SetupFIFOS(LPC_UART0, (UART_FCR_FIFO_EN | UART_FCR_TRG_LEV2));
	Chip_UART_SetBaudFDR(LPC_UART0, DEFAULT_BAUDRATE);
	Chip_UART_TXEnable(LPC_UART0);
	Chip_UART_Init(LPC_UART2);
	Chip_UART_ConfigData(LPC_UART2, (UART_LCR_WLEN8 | UART_LCR_SBS_1BIT));
	Chip_UART_SetupFIFOS(LPC_UART2, (UART_FCR_FIFO_EN | UART_FCR_TRG_LEV2));
	Chip_UART_SetBaudFDR(LPC_UART2, DEFAULT_BAUDRATE);
	Chip_UART_TXEnable(LPC_UART2);
	uart0SetRTSCTSEnable(TRUE);
	uart1SetRTSCTSEnable(TRUE);
	LPC_UART0->IER = 0x01;	// enable UART receive interrupts (tx interrupts will get enable when transmit data available)
	LPC_UART2->IER = 0x01;	// enable UART receive interrupts (tx interrupts will get enable when transmit data available)
	NVIC_EnableIRQ(UART0_IRQn);
	NVIC_EnableIRQ(UART2_IRQn);
// *****************************************************************************  
	UARTCommandModeActive = 0;

// *****************************************************************************  
	uart0OutBufferWritePointer = 0;
	uart0OutBufferReadPointer = 0;

	uart0TimeoutValue = 0;

	uart1OutBufferWritePointer = 0;
	uart1OutBufferReadPointer = 0;
	uart1TimeoutValue = 0;

// *****************************************************************************  
	commandLine[0] = 0;
	commandLinePointer = 0;

	TCPpointer0 = 0;
	uart0TCPNextBlockToSend = 0;

	TCPpointer1 = 0;
	uart1TCPNextBlockToSend = 0;

	for (n = 0; n < TCP_BUF_COUNT; n++) {			// initially assume all buffers get filled completely
		TCPBufferLength0[n] = TCP_PACKET_SIZE;
		TCPBufferLength1[n] = TCP_PACKET_SIZE;
	}
	xdev_out(putcharUART0);
}

// *****************************************************************************  
#define UARTReturn()	   {xputc('\n');}

// *****************************************************************************  
void uartShowVersion(void) {
	(void) strcat(commandReturn, "\n");
	(void) strcat(commandReturn, WLAN_WELCOME_MESSAGE);

#ifdef INCLUDE_RP_FIRMWARE_UPGRADE
	(void) strcat(commandReturn, " - Firmware Upgrade included -\n");
#endif
	(void) strcat(commandReturn, " - UART1 enabled -\n");
#ifdef AUTO_CONNECT_TCP
	(void) strcat(commandReturn, " - AutoConnect TCP -\n");
#endif
#ifdef AUTO_CONNECT_UDP
	(void) strcat(commandReturn, " - AutoConnect UDP -\n");
#endif

	(void) strcat(commandReturn, "\n");
}

void uartShowUsage(void) {
	*commandReturn = 0;
	uartShowVersion();
	(void) strcat(commandReturn, "Available commands:\n");
	(void) strcat(commandReturn, "  -r          Hardware Reset of WLAN module\n");
	(void) strcat(commandReturn, "  -b          Boot Module\n");
	(void) strcat(commandReturn, "  -i          Init module (attach to Network)\n");
	(void) strcat(commandReturn, "  -c          Reset/Reopen socket connections\n");
	(void) strcat(commandReturn, "  -?          Query IP/MAC address of Module\n");
	(void) strcat(commandReturn, "  -f          Query Firmware Version\n");
#ifdef INCLUDE_RP_FIRMWARE_UPGRADE
	(void) strcat(commandReturn, "  -u          Update Firmware\n");
#endif
	(void) strcat(commandReturn, "\n");
	(void) strcat(commandReturn, "  !SB<p>=<b>  Set baud rate of port p[UART-ID or IP-port] to y\n");
	(void) strcat(commandReturn, "  +/-         enable/disable echo in command mode\n");
	(void) strcat(commandReturn, "  P           Enter reprogramming mode\n");
	(void) strcat(commandReturn, "  R           Reset microcontroller\n");
	(void) strcat(commandReturn, "  0,1,2       Led off, on, blinking\n");
	if (UARTCommandModeActive) {
		(void) strcat(commandReturn, "  ///         Leave Command Mode\n");
	}
	(void) strcat(commandReturn, "\n");
}

// *****************************************************************************
uint32_t parseULong(char **c) {
	uint32_t ul = 0;
	while (((**c) >= '0') && ((**c) <= '9')) {
		ul = 10 * ul;
		ul += ((**c) - '0');
		(*(c))++;
	}
	return (ul);
}

extern void RP_FirmwareUpgrade(void);
// *****************************************************************************
// * ** parseCommand (from UART or CTRL-Sockets) ** */
// *****************************************************************************
int32_t parseCommand(char *c) {

	switch (*c) {
#ifdef DEBUG_WLAN_STRESSTEST
	case 'D':
	case 'd':
		if ((*(c + 1)) == '+') {
			DEBUG_WLAN_ENABLE = 1;
		} else {
			DEBUG_WLAN_ENABLE = 0;
		}
		UARTCommandModeActive = 0;
		break;
#endif

	case '!':
		c++;
		goto parseSetCommand;
	case '?':
		c++;
		goto parseGetCommand;
	case '-':
		c++;
		if ((*c) == 0) {								// only '-' --> disable output
			commandModeEchoActive = 0;
			break;
		}
		goto parseWLANCommand;
	case '+':
		commandModeEchoActive = 1;
		break;

	case 'P':
	case 'p':
		enterReprogrammingMode();
		break;
	case 'R':
	case 'r':
		resetDevice();
		break;

	case '0':
		LEDSetOff();
		break;
	case '1':
		LEDSetOn();
		break;
	case '2':
		LEDSetBlinking();
		break;

	case '/':
		if ((*(c + 1) == '/') && (*(c + 2) == '/')) {
			UARTCommandModeActive = 0;
			(void) strcpy(commandReturn, (char*) "Leaving UART0 CMD mode!\n");
			return (1);
		}
		break;

	default:
		(void) strcpy(commandReturn, "?\n");
		return (1);
	}
	return (0);

// *****************************************************************************
// * ** parseSetCommand ** */
// *****************************************************************************
	parseSetCommand: switch (*c) {
	case 'S':  	  					// !SBp=b
	case 's': {
		int32_t id, baudRate;
		c += 2;
		id = parseULong(&c);
		c++;
		baudRate = parseULong(&c);
		if ((id == 0) || (id == (BASE_PORT + TCP_PORT_OFFSET)) || (id == (BASE_PORT + UDP_PORT_OFFSET)) || (id == (BASE_PORT + TCP_REPROG_PORT_OFFSET))) {
			uart0WaitForTransferDone();
			uart0SetBaudRate(baudRate);
		}
		if ((id == 1) || (id == (BASE_PORT + TCP_PORT_OFFSET + UART1_PORT_OFFSET)) || (id == (BASE_PORT + UDP_PORT_OFFSET + UART1_PORT_OFFSET))
				|| (id == (BASE_PORT + TCP_REPROG_PORT_OFFSET + UART1_PORT_OFFSET))) {
			uart1WaitForTransferDone();
			uart1SetBaudRate(baudRate);
		}
	}
		break;

	default:
		(void) strcpy(commandReturn, "unknown Set command!\n");
		return (1);
	}
	return (0);

// *****************************************************************************
// * ** parseGetCommand ** */
// *****************************************************************************
	parseGetCommand: switch (*c) {
	case '?':
		uartShowUsage();
		return (1);
	}
	(void) strcpy(commandReturn, "unknown Get command!\n");
	return (1);

// *****************************************************************************
// * ** parseWLANCommand ** */
// *****************************************************************************
	parseWLANCommand: switch (*c) {
	case 'r':
		RP_HardwareReset();
		break;

	case 'b':
		RP_BootModule();
		break;

	case 'i':
		RP_InitModule();
		break;

	case '?':
		(void) xsprintf(commandReturn, "IP:  %3d.%3d.%3d.%3d\n", (myCurrentIPAddress >> 24), (myCurrentIPAddress >> 16) & 0xFF,
				(myCurrentIPAddress >> 8) & 0xFF, (myCurrentIPAddress) & 0xFF);

		(void) xsprintf(commandReturn, "%sMAC: %02X:%02X:%02X:%02X:%02X:%02X\n", commandReturn, myMACAddress[0], myMACAddress[1], myMACAddress[2],
				myMACAddress[3], myMACAddress[4], myMACAddress[5]);
		return (1);

	case 'f':
		(void) strcpy(commandReturn, "FW: ");
		(void) RP_ModuleFWVersion(commandReturn + 4);
		(void) strcat(commandReturn, "\n");
		return (1);

	case 'c':
		socketHandleTCP0 = 0;
		socketHandleUDP0 = 0;
		socketHandleTCPRP0 = 0;
		socketHandleTCP1 = 0;
		socketHandleUDP1 =0;
		socketHandleTCPRP1 = 0;
		socketHandleCTRLTCP = 0;
		socketHandleCTRLUDP = 0;
		break;

#ifdef INCLUDE_RP_FIRMWARE_UPGRADE
		case 'u':
		RP_FirmwareUpgrade();
		break;
#endif

	case '0':
		(void) RP_i0();
		break;
	case '1':
		(void) RP_i1();
		break;
	case '2':
		(void) RP_i2();
		break;
	case '3':
		(void) RP_i3();
		break;
	case '4':
		(void) RP_i4();
		break;
	case '5':
		(void) RP_i5();
		break;
	case '6':
		(void) RP_i6();
		break;
	case '7':
		(void) RP_i7();
		break;
	case '8':
		(void) RP_i8();
		break;
	case '9':
		(void) RP_i9();
		break;

	default:
		(void) strcpy(commandReturn, "unknown WLAN command!\n");
		return (1);
	}

	return (0);
}

// *****************************************************************************
// * ** UARTParseNewChar ** */
// *****************************************************************************
void uartParseNewChar(uint8_t newChar) {

	switch (newChar) {
	case 8:			// backspace
		if (commandLinePointer > 0) {
			commandLinePointer--;
			if (commandModeEchoActive) {
				xputc(8);
				xputc(' ');
				xputc(8);
			}
		}
		break;

	case 10:
	case 13:
		if (commandModeEchoActive) {
			UARTReturn()
			;
		}
		if (commandLinePointer > 0) {
			commandLine[commandLinePointer] = 0;
			if (parseCommand(commandLine)) {
				xprintf("%s", commandReturn);
			}
			commandLinePointer = 0;
		}
		break;

	default:
		if (commandLinePointer < (COMMAND_LINE_MAX_LENGTH - 2)) {
			if (commandModeEchoActive) {
				xputc(newChar);	  		   	// echo to indicate int8_t arrived
			}
			commandLine[commandLinePointer] = newChar;
			commandLinePointer++;
		} else {
			commandLinePointer = 0;
		}
	}  // end of switch

}  // end of UARTParseNewChar

// *****************************************************************************
// ** Shortcut definitions
// *****************************************************************************
#define ADD_TO_TCPQUEUE0(x)		{TCPqueue0[TCPpointer0] = (x); TCPpointer0++; TCPpointer0 &= TCP_BUF_TOTAL_SIZE_MASK;}
#define ADD_TO_TCPQUEUE1(x)		{TCPqueue1[TCPpointer1] = (x); TCPpointer1++; TCPpointer1 &= TCP_BUF_TOTAL_SIZE_MASK;}

// *****************************************************************************
// *****************************************************************************
void uartIterate(void) {
	int32_t emptyBufferCount;
	int32_t error;
	uint32_t bufIndex;

#ifdef DEBUG_WLAN_STRESSTEST
// WLAN UART DEBUG generate some "random" packets:
	{
		if ((DEBUG_WLAN_ENABLE) && (UARTCommandModeActive == 0) && (uart0ActiveSocket != 0)) {
			int32_t emptyBufferCount;
			emptyBufferCount = (uart0TCPNextBlockToSend - TCP_BUF_INDEX(TCPpointer0)) - 1;
			if (emptyBufferCount <= 0)
				emptyBufferCount += TCP_BUF_COUNT;
			if (emptyBufferCount > 4) {
				uint32_t n;
				for (n = 0; n < 64; n++) {
					uint32_t t = Chip_RIT_GetCounter(LPC_RITIMER);
					debugCounter++;

					ADD_TO_TCPQUEUE0((debugCounter & 0xFF00) >> 8);	// event 1st byte
					ADD_TO_TCPQUEUE0((debugCounter & 0xFF));	// event 2nd byte
					ADD_TO_TCPQUEUE0((t & 0xFF000000) >> 24);	// timestamp top byte
					ADD_TO_TCPQUEUE0((t & 0x00FF0000) >> 16);	// timestamp 2nd byte
					ADD_TO_TCPQUEUE0((t & 0x0000FF00) >> 8);	// timestamp 3rd byte
					ADD_TO_TCPQUEUE0((t & 0x000000FF));	// timestamp final byte

					ADD_TO_TCPQUEUE0(0x7F);	// send dummy event 0x7FFF.FFFFFFFF for immediate synch
					ADD_TO_TCPQUEUE0(0xFF);
					ADD_TO_TCPQUEUE0(0xFF);
					ADD_TO_TCPQUEUE0(0xFF);
					ADD_TO_TCPQUEUE0(0xFF);
					ADD_TO_TCPQUEUE0(0xFF);
				}
				// check buffer fill level (for rts/cts)
				emptyBufferCount = (uart0TCPNextBlockToSend - TCP_BUF_INDEX(TCPpointer0)) - 1;
				if (emptyBufferCount <= 0)
					emptyBufferCount += TCP_BUF_COUNT;
				if ((emptyBufferCount > 0) && (emptyBufferCount <= 4)) {
					Chip_GPIO_SetPinOutHigh(LPC_GPIO, UART0_CTS_PORT, UART0_CTS_PIN);  // set CTS pin to high -> no more data, please.
				}
				uart0TimeoutValue = 0;
			}
		}
	}
#endif

// *****************************************************************************
// **************************************** start sending buffered data to UART0
	if (LPC_UART0->IER == 0x01) {							// only receive interrupt active... -> sending is not active
		if (uart0OutBufferWritePointer != uart0OutBufferReadPointer) { // do we have data to send?
			(void) uart0InitiateInterruptSending();
		}
	}

// *****************************************************************************
// *********************************** check timeout for TCP-packages from UART0
	LPC_UART0->IER &= ~(0x01);				   // stop receiving interrupt
	if (TCPpointer0 & TCP_PACKET_SIZE_MASK) {					// only start timeout counter if not at beginning of block

		uart0TimeoutValue++;
		if (uart0TimeoutValue & 0x400) {	  					// check for timeout
			uart0TimeoutValue = 0;			 						// ok, we have reached timeout

			bufIndex = TCP_BUF_INDEX(TCPpointer0);				// which block are we pointing at?

			TCPBufferLength0[bufIndex] = TCPpointer0 - TCP_BUFFER_START(bufIndex);   // this is how many bytes we have in our partially filled buffer

			bufIndex++;			   								// advance pointer to next block
			if (bufIndex == TCP_BUF_COUNT) {
				bufIndex = 0;
			}
			TCPpointer0 = TCP_BUFFER_START(bufIndex);	   			// point to beginning of next block
		}
	}
	LPC_UART0->IER |= (0x01);				   // enable receiving interrupt

// *****************************************************************************
// ****************************************** start sending UART0 data to Socket
	if (TCP_BUF_INDEX(TCPpointer0) != uart0TCPNextBlockToSend) {
		DEBUGOUT3("UART_TCP_Iterate0: Sending block %2d from addr: %3d\n", uart0TCPNextBlockToSend, TCP_BUFFER_START(uart0TCPNextBlockToSend));

		if (UARTCommandModeActive) {	   // parse UART data as command
			int32_t n;
			for (n = 0; n < TCPBufferLength0[uart0TCPNextBlockToSend]; n++) {
				uartParseNewChar(*(TCPqueue0 + TCP_BUFFER_START(uart0TCPNextBlockToSend) + n));
			}
			TCPBufferLength0[uart0TCPNextBlockToSend] = TCP_PACKET_SIZE;							// restore default buffer size
			uart0TCPNextBlockToSend++;
			if (uart0TCPNextBlockToSend >= TCP_BUF_COUNT)
				uart0TCPNextBlockToSend = 0;

		} else { // not in command mode

			if (uart0ActiveSocket != 0) {   // send off package
				if (uart0ActiveSocket == socketHandleUDP0) {
					error = RPSendUDPDataPacket(uart0ActiveSocket, ipAddressMemory0, BASE_PORT + UDP_PORT_OFFSET,
							TCPqueue0 + TCP_BUFFER_START(uart0TCPNextBlockToSend), TCPBufferLength0[uart0TCPNextBlockToSend]);
				} else {  // this can be TCP or TCPRP
					error = RPSendTCPDataPacket(uart0ActiveSocket, TCPqueue0 + TCP_BUFFER_START(uart0TCPNextBlockToSend),
							TCPBufferLength0[uart0TCPNextBlockToSend]);
				}
				if (error >= 0) {
					TCPBufferLength0[uart0TCPNextBlockToSend] = TCP_PACKET_SIZE;						// restore default buffer size
					uart0TCPNextBlockToSend++;
					if (uart0TCPNextBlockToSend >= TCP_BUF_COUNT)
						uart0TCPNextBlockToSend = 0;
					DEBUGOUT2("UART_TCP_Iterate0: next block to send: %2d\n", uart0TCPNextBlockToSend);
				}
			} else {
				if (TCPBufferLength0[uart0TCPNextBlockToSend] == 3) {
					char *c = TCPqueue0 + TCP_BUFFER_START(uart0TCPNextBlockToSend);
					if ((*(c) == '/') && (*(c + 1) == '/') && (*(c + 2) == '/')) {
						// here we received a block of three '/' within an empty sequence --> go to command mode!
						UARTCommandModeActive = 1;
						*commandReturn = 0;	  	  	// clear return message; otherwise "old" message is still in buffer
						uartShowVersion();
						xprintf("%s", commandReturn);
					}
				}
				if (UARTCommandModeActive != 0) {
					DEBUGOUT1("UART_TCP_Iterate0: received UART data, but no connected socket\n");
				}
				TCPBufferLength0[uart0TCPNextBlockToSend] = TCP_PACKET_SIZE;							// restore default buffer size
				uart0TCPNextBlockToSend++;
				if (uart0TCPNextBlockToSend >= TCP_BUF_COUNT)
					uart0TCPNextBlockToSend = 0;
			}
		}

		emptyBufferCount = (uart0TCPNextBlockToSend - TCP_BUF_INDEX(TCPpointer0)) - 1;
		if (emptyBufferCount < 0)
			emptyBufferCount += TCP_BUF_COUNT;
		DEBUGOUT2("UART0 - emptyBuffer: %d\n", emptyBufferCount);
		if (emptyBufferCount >= 8) {
			Chip_GPIO_SetPinOutLow(LPC_GPIO, UART0_CTS_PORT, UART0_CTS_PIN);  // set CTS pin to low -> allow new data
		}
	}

// *****************************************************************************
// **************************************** start sending buffered data to UART1
	if (LPC_UART2->IER == 0x01) {							// only receive interrupt active... -> sending is not active
		if (uart1OutBufferWritePointer != uart1OutBufferReadPointer) { // do we have data to send?
			(void) uart1InitiateInterruptSending();
		}
	}

// *****************************************************************************
// *********************************** check timeout for TCP-packages from UART1
	LPC_UART2->IER &= ~(0x01); // stop receiving interrupt
	if (TCPpointer1 & TCP_PACKET_SIZE_MASK) {					// only start timeout counter if not at beginning of block

		uart1TimeoutValue++;
		if (uart1TimeoutValue & 0x400) {	  					// check for timeout
			uart1TimeoutValue = 0;	  					// ok, we have reached timeout

			bufIndex = TCP_BUF_INDEX(TCPpointer1);	  					// which block are we pointing at?

			TCPBufferLength1[bufIndex] = TCPpointer1 - TCP_BUFFER_START(bufIndex);	  			// this is how many bytes we have in our partially filled buffer

			bufIndex++;	  					// advance pointer to next block
			if (bufIndex == TCP_BUF_COUNT) {
				bufIndex = 0;
			}
			TCPpointer1 = TCP_BUFFER_START(bufIndex);	   			// point to beginning of next block
		}
	}
	LPC_UART2->IER |= (0x01);				   // enable receiving interrupt

// *****************************************************************************
// ****************************************** start sending UART1 data to Socket
	if (TCP_BUF_INDEX(TCPpointer1) != uart1TCPNextBlockToSend) {
		DEBUGOUT3("UART_TCP_Iterate1: Sending block %2d from addr: %3d\n", uart1TCPNextBlockToSend, TCP_BUFFER_START(uart1TCPNextBlockToSend));

		if (uart1ActiveSocket != 0) {   // send off package
			if (uart1ActiveSocket == socketHandleUDP1) {
				error = RPSendUDPDataPacket(uart1ActiveSocket, ipAddressMemory1, BASE_PORT + UDP_PORT_OFFSET + UART1_PORT_OFFSET,
						TCPqueue1 + TCP_BUFFER_START(uart1TCPNextBlockToSend), TCPBufferLength1[uart1TCPNextBlockToSend]);
			} else {  // this can be TCP or TCPRP
				error = RPSendTCPDataPacket(uart1ActiveSocket, TCPqueue1 + TCP_BUFFER_START(uart1TCPNextBlockToSend),
						TCPBufferLength1[uart1TCPNextBlockToSend]);
			}
			if (error >= 0) {
				TCPBufferLength1[uart1TCPNextBlockToSend] = TCP_PACKET_SIZE;						// restore default buffer size
				uart1TCPNextBlockToSend++;
				if (uart1TCPNextBlockToSend >= TCP_BUF_COUNT)
					uart1TCPNextBlockToSend = 0;
				DEBUGOUT2("UART_TCP_Iterate-1: next block to send: %2d\n", uart1TCPNextBlockToSend);
			}
		} else {
			DEBUGOUT1("UART_TCP_Iterate-1: received UART data, but no connected socket\n");
			TCPBufferLength1[uart1TCPNextBlockToSend] = TCP_PACKET_SIZE;							// restore default buffer size
			uart1TCPNextBlockToSend++;
			if (uart1TCPNextBlockToSend >= TCP_BUF_COUNT)
				uart1TCPNextBlockToSend = 0;
		}

		emptyBufferCount = (uart1TCPNextBlockToSend - TCP_BUF_INDEX(TCPpointer1)) - 1;
		if (emptyBufferCount < 0)
			emptyBufferCount += TCP_BUF_COUNT;
		DEBUGOUT2("UART1 - emptyBuffer: %d\n", emptyBufferCount);
		if (emptyBufferCount >= 8) {
			Chip_GPIO_SetPinOutLow(LPC_GPIO, UART2_CTS_PORT, UART2_CTS_PIN);  // set CTS pin to low -> allow new data
		}
	}

}

