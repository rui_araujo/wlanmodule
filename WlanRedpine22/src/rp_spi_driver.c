/*
 * rp_spi_driver.c
 *
 *  Created on: Jun 4, 2014
 *      Author: raraujo
 */
#include "chip.h"
#include "rp_spi_driver.h"
#include "rp_pinout.h"
#include "RPinterface.h"
#include <string.h>

//#define REDPINE_STARTTOKEN_TIMEOUT		0xFFFF 			// this many reads to wait for start token confirmation
#define REDPINE_STARTTOKEN_TIMEOUT		0x3FFF 			// this many reads to wait for start token confirmation

// *****************************************************************************
unsigned char RPSPI_FrameHeader[16];
unsigned char RPSPI_DataFrameCommandHeader[2];
unsigned char RPSPI_TransmitBuffer[1600];
unsigned char RPSPI_ReceiveBuffer[1600];

Chip_SSP_DATA_SETUP_T xf_setup;
char rx, tx;
void SPI_Init() {
	xf_setup.rx_data = &rx;
	xf_setup.tx_data = &tx;
	Chip_SSP_Init(LPC_SSP1);
	Chip_SSP_SetBitRate(LPC_SSP1, 25000000);
	Chip_SSP_Enable(LPC_SSP1);
	Chip_GPIO_SetPinState(LPC_GPIO, WLAN_RESET_PORT, WLAN_RESET_PIN, 1); //set reset to high
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, WLAN_RESET_PORT, WLAN_RESET_PIN);
	Chip_GPIO_SetPinDIRInput(LPC_GPIO, INTERRUPT_PORT, INTERRUPT_PIN);
}

int32_t RPSPI_SendCMD(uint32_t cmd) {
	unsigned char reply;

	(void) RPSPI_exchange(cmd & 0xFF);		// send lowest byte, ignore readback

	cmd >>= 8;									// prepare and send next byte
	if ((reply = RPSPI_exchange(cmd & 0xFF)) != RPSPI_RESPONSE_OK) {
		DEBUGOUT2("RPSPI_SendCMD error (C2): 0x%02X!\n", reply);
		return (-1);
	}

	cmd >>= 8;									// prepare and send next byte
	if ((reply = RPSPI_exchange(cmd & 0xFF)) != RPSPI_RESPONSE_OK) {
		DEBUGOUT2("RPSPI_SendCMD error (C3): 0x%02X!\n", reply);
		return (-2);
	}

	cmd >>= 8;									// prepare and send next byte
	if ((reply = RPSPI_exchange(cmd & 0xFF)) != RPSPI_RESPONSE_OK) {
		DEBUGOUT2("RPSPI_SendCMD error (C4): 0x%02X!\n", reply);
		return (-3);
	}
	return (0);
}

// *****************************************************************************
// ** Send addr (32bit)
// *****************************************************************************
int32_t RPSPI_SendAddr(uint32_t addr) {
	unsigned char reply;

	if ((reply = RPSPI_exchange(addr & 0xFF)) != RPSPI_RESPONSE_OK) {
		DEBUGOUT2("RPSPI_SendAddr error (A0): 0x%02X!\n", reply);
		return (-1);
	}

	addr >>= 8;									// prepare and send next byte
	if ((reply = RPSPI_exchange(addr & 0xFF)) != RPSPI_RESPONSE_OK) {
		DEBUGOUT2("RPSPI_SendAddr error (A1): 0x%02X!\n", reply);
		return (-1);
	}

	addr >>= 8;									// prepare and send next byte
	if ((reply = RPSPI_exchange(addr & 0xFF)) != RPSPI_RESPONSE_OK) {
		DEBUGOUT2("RPSPI_SendAddr error (A2): 0x%02X!\n", reply);
		return (-2);
	}

	addr >>= 8;									// prepare and send next byte
	if ((reply = RPSPI_exchange(addr & 0xFF)) != RPSPI_RESPONSE_OK) {
		DEBUGOUT2("RPSPI_SendAddr error (A3): 0x%02X!\n", reply);
		return (-3);
	}

	return (0);
}

// *****************************************************************************
// ** Send special wakeup sequento to RP Module **
// *****************************************************************************
int32_t RPSPI_WakeupInterface(void) {
	int32_t reply;
	(void) RPSPI_exchange(0x15);
	reply = RPSPI_exchange(0x00);
	if (reply != RPSPI_RESPONSE_OK) {
		DEBUGOUT2("error initializing SPI interface on WLAN module, error code: %2X!\n\r",reply);
		return (-1);
	}
	return (0);
}
unsigned char RPSPI_exchange(unsigned char d_out) {
	/*Chip_SSP_SendFrame(LPC_SSP1, d_out);
	 while (Chip_SSP_GetStatus(LPC_SSP1, SSP_STAT_RNE) == RESET) {
	 ;
	 }
	 return Chip_SSP_ReceiveFrame(LPC_SSP1);*/
	xf_setup.length = 1;
	xf_setup.rx_cnt = 0;
	xf_setup.tx_cnt = 0;
	tx = d_out;
	Chip_SSP_RWFrames_Blocking(LPC_SSP1, &xf_setup);
	return rx;
}

// *****************************************************************************
// ** Library to transfer data host <-> module
// *****************************************************************************
//
// cmd = C1C2C3C4:
//       C1, 7:6 - command type: [00] init; [01] read/write
//       C1,   5 -                               0:read; 1:write
//       C1,   4 - 0: RP WLAN module internal access; 1: bus access (data)
//       C1,   3 - 0: Master Access (->Memory R/W); 1: Slave Access (->Frame R/W)
//       C1,   2 - 1: 16 bit length of transfer,  0: 2-bit encoded length of transfer
//       C1, 1:0 -                                --> 2 bit length of transfer

//       C2, 7:6 - [00]: 8bit mode;  [01]: 32bit mode
//       C2, 5:0 - internal address, or type of packet (2:regular data; 4:management)

//       C3, 7:0 - length of data, lst ( 7:0)
//       C4, 7:0 - length of data, msb (15:8)

// --> Management Packet Write: C1=0111.1100 (0x7C);  C2=0000.0100 (0x04)
//                              C3/4: length = 7Words (14 bytes) + data

// *****************************************************************************
// ** Register read **
// *****************************************************************************
//Base Address: 0x0800_0000 Address Offset Register Description
//0x00 SPI_HOST_INTR
//0x08 SPI_MODE

//Base Address: 0x2200_0000 Address Offset Register Description
//0x08 INTR_MASK
//0x10 INTR_CLEAR

// *****************************************************************************
// *****************************************************************************
int32_t RPSPI_SPIRegisterWrite(uint32_t offset, uint32_t data) {

	int32_t reply;

	DEBUGOUT1("RPSPI_SPIRegisterWrite, sending cmd header\n");
	(void) RPSPI_exchange(0x61);

	if ((reply = RPSPI_exchange(offset & 0x3F)) != RPSPI_RESPONSE_OK) {
		DEBUGOUT2("RPSPI_SPIRegisterWrite error (C2): 0x%02X!\n", reply);
		return (-1);
	}

	reply = RPSPI_exchange(data & 0xFF);	 	// send new register data

	DEBUGOUT1("RPSPI_SPIRegisterWrite, done!\n");

	return (0);
}

// *****************************************************************************
int32_t RPSPI_SPIRegisterRead(uint32_t offset) {
	int32_t reply;
	uint32_t timeout;

	DEBUGOUT1("RPSPI_SPIRegisterRead, sending cmd header\n");
	(void) RPSPI_exchange(0x41);			// send lowest byte, ignore readback

	if ((reply = RPSPI_exchange(offset & 0x3F)) != RPSPI_RESPONSE_OK) {
		DEBUGOUT2("RPSPI_SPIRegisterRead error (C2): 0x%02X!\n", reply);
		return (-1);
	}

	DEBUGOUT1("RPSPI_SPIRegisterRead, waiting for start token...\n");
	reply = 0;
	timeout = REDPINE_STARTTOKEN_TIMEOUT; // timeout value empirically determined
	while ((reply != 0x55) && (timeout)) {	  	// wait for start token
		timeout--;
		reply = RPSPI_exchange(0x00);
	}
	if (timeout == 0) {
		DEBUGOUT1("RPSPI_SPIRegisterRead, timeout waiting for start token...\n");
		return (-1);
	}
	reply = RPSPI_exchange(0x00); // we have received a start token, now fetch header

	DEBUGOUT2("RPSPI_SPIRegisterRead, read 0x%02X...\n", reply);

	return (reply);
}

// *****************************************************************************
// *****************************************************************************
int32_t RPSPI_MemoryWrite(uint32_t addr, unsigned char *data, uint32_t dataLength) {
	uint32_t cmd;

	if ((dataLength & 0x03) != 0) {
		DEBUGOUT1("RPSPIMemWrite, need multiple of 4 bytes!\n");
		return (-1);
	}

	cmd = 0x0074;									// -> write memory
	cmd |= (dataLength & 0xFFFF) << 16;				// length msb,lsb

	DEBUGOUT1("RPSPIMemWrite, sending cmd header\n");
	if (RPSPI_SendCMD(cmd) < 0) {
		return (-1);
	}

	DEBUGOUT1("RPSPIMemWrite, sending address\n");
	if (RPSPI_SendAddr(addr) < 0) {
		return (-2);
	}

	DEBUGOUT1("RPSPIMemWrite, starting payload\n");
	while (dataLength) {	  	// now send all data (treat as uint32_t!)
		(void) RPSPI_exchange(*data);
		data++;
		dataLength--;
	}

	DEBUGOUT1("RPSPIMemWrite, done\n");
	return (0);
}

// *****************************************************************************
int32_t RPSPI_FrameWrite(uint32_t cmd, unsigned char *data, uint32_t dataLength) {
	int32_t n;
	uint32_t cmd_fd;

	if ((dataLength & 0x03) != 0) {
		DEBUGOUT1("Warning: RPSPIFrameWrite needs multiple of 4 bytes!\n");
		return (-1);
	}

	DEBUGOUT1("RPSPIFrameWrite, sending FD cmd header\n");
	cmd_fd = cmd & 0xFFFF;
	cmd_fd |= ((16) << 16);	// here we send only the frame descriptor, so add length of frame descriptor (16 bytes)
	if (RPSPI_SendCMD(cmd_fd) < 0) {
		return (-1);
	}

	DEBUGOUT1("RPSPIFrameWrite, sending frame descriptor\n");
	for (n = 0; n < 16; n++) {
		(void) RPSPI_exchange(RPSPI_FrameHeader[n]);
	}

	if (dataLength == 0) {
		DEBUGOUT1("RPSPIFrameWrite, no data package - done\n");
		return (0);
	}

	// ***************************************************************************
	// ***************************************************************************
	for (n = 256; n; n--) {
	};		   // we need some delay here!

	DEBUGOUT1("RPSPIFrameWrite, sending cmd header\n");
	if (RPSPI_SendCMD(cmd) < 0) {
		return (-1);
	}

	if (RPSPI_FrameHeader[14] == 0x02) { // this is a data package (no management), send extra header!
		DEBUGOUT1("RPSPIFrameWrite, sending DataCommand Header\n");
		(void) RPSPI_exchange(RPSPI_DataFrameCommandHeader[0]);
		(void) RPSPI_exchange(RPSPI_DataFrameCommandHeader[1]);
		dataLength -= 2;			// send two fewer bytes of data
	}

	if (dataLength) {
		DEBUGOUT1("RPSPIFrameWrite, starting payload\n");
		while (dataLength) {	  // now send all data (treat as uint32_t!)
			(void) RPSPI_exchange(*data);
			data++;
			dataLength--;
		}
	} else {
		DEBUGOUT1("RPSPIFrameWrite, no payload\n");
	}

	DEBUGOUT1("RPSPIFrameWrite, done\n");
	return (0);
}

// *****************************************************************************
int32_t RPSPI_ManagementFrameWrite(unsigned char frameType, unsigned char *data, uint32_t dataLength) {
	uint32_t cmd;
	int32_t reply;

	DEBUGOUT1("RPSPIManagementFrameWrite:\n");

	/* old data sheet: C2, bits 5:0 --- Otherwise, it carries miscellaneous information for the RS9110-N-11-2X module. These bits are used to indicate type of the packet, flowing from the Host to the RS9110-N-11-2X module. Definition is as follows:
	 2 - Regular data packet;  4 - Management packet. */
	cmd = 0x047C; 							// -> write management package
	cmd |= (((dataLength) & 0xFFFF) << 16);	// total length (frame header(16)+payload) msb,lsb

	memset(RPSPI_FrameHeader, 0, 16);	// clear SPIRP_FrameHeader, construct new header
	if (dataLength < 255) {
		RPSPI_FrameHeader[0] = dataLength;	// encode length of payload directly in W0[7:0]
	} else {
		RPSPI_FrameHeader[0] = 0xFF;		   		  	// signal int32_t data
		RPSPI_FrameHeader[8] = (dataLength & 0xFF);	// length of data (lsb) in W4[ 7:0]
		RPSPI_FrameHeader[9] = ((dataLength & 0xFF00) >> 8);	// length of data (msb) in W4[15:8]
	}

	RPSPI_FrameHeader[1] = frameType;		// W0 [15:8], command, here "INIT"
	RPSPI_FrameHeader[14] = 0x04;				// W7 [ 7:0], management frame

	reply = RPSPI_FrameWrite(cmd, data, dataLength);
	return (reply);
}

// *****************************************************************************
int32_t RPSPI_DataFrameWrite(unsigned char frameType, unsigned char *data, uint32_t dataLength) {
	uint32_t cmd;
	int32_t reply;
	uint32_t fillIn, dataLengthSPIFillIn;

	DEBUGOUT1("RPSPITCPDataFrameWrite:\n");
	dataLength += 2;	// we will send 2 extra bytes as DataFrameCommandHeader

	fillIn = (4 - ((dataLength) & 0x03)) & 0x03;	// check if data length is multiple of four
	dataLengthSPIFillIn = dataLength + fillIn;
	DEBUGOUT2("RPSendTCPDataPacket: fillIn=%d\n", fillIn);
	DEBUGOUT2("RPSendTCPDataPacket: packetLengthSPIFillIn=%d\n",dataLengthSPIFillIn);

	/* old data sheet: C2, bits 5:0 --- Otherwise, it carries miscellaneous information for the RS9110-N-11-2X module. These bits are used to indicate type of the packet, flowing from the Host to the RS9110-N-11-2X module. Definition is as follows:
	 2 - Regular data packet;  4 - Management packet. */
	cmd = 0x027C;								// -> write data package
	cmd |= (((dataLengthSPIFillIn) & 0xFFFF) << 16);								// total length (data frame command header + payload) msb,lsb
	DEBUGOUT3("RPSendTCPDataPacket: CMD=%d = %08x\n", cmd, cmd);

	memset(RPSPI_FrameHeader, 0, 16);	// clear RPSPI_FrameHeader, construct new header

	RPSPI_FrameHeader[0] = (dataLength & 0xFF); 	// W0 [ 7:0] data length lsb
	RPSPI_FrameHeader[1] = (dataLength & 0x0F00) >> 8; // W1 [11:8] data length msb
	RPSPI_FrameHeader[14] = 0x02;						// W7 [ 7:0], data frame

	RPSPI_DataFrameCommandHeader[0] = frameType;
	RPSPI_DataFrameCommandHeader[1] = 0;

	reply = RPSPI_FrameWrite(cmd, data, dataLengthSPIFillIn);
	return (reply);
}

// *****************************************************************************
// *****************************************************************************
int32_t RPSPI_FrameRead(uint32_t cmd, unsigned char *dataSpace, uint32_t maxDataLength) {		// maxDataLength is the **maximal** allowed number of bytes to be read
	// true number of bytes determined by frame header!
	int32_t reply;
	int32_t headerCount;
	unsigned char *dataSpaceWritePointer;
	uint32_t dataCount, bytesRequested;
	uint32_t timeout;

	if ((maxDataLength & 0x03) != 0) {
		DEBUGOUT1("RPSPI_FrameRead requires multiple of 4 bytes!\n");
		return (-1);
	}

	DEBUGOUT2("RPSPIFrameRead-Header, sending cmd header (0x%08lX)\n", cmd);
	if (RPSPI_SendCMD(cmd) < 0) {
		return (-1);
	}

	DEBUGOUT1("RPSPIFrameRead-Header, waiting for start token...\n");
	reply = 0;
	timeout = REDPINE_STARTTOKEN_TIMEOUT; // timeout value empirically determined
	while ((reply != 0x55) && (timeout)) {	  	// wait for start token
		timeout--;
		reply = RPSPI_exchange(0x00);
	}
	if (timeout == 0) {
		DEBUGOUT1("RPSPIFrameRead-Header, timeout waiting for start token...\n");
		return (-1);
	}

	// we have received a start token, now fetch header
	DEBUGOUT1("RPSPIFrameRead-Header, retrieving header...\n");
	dataSpaceWritePointer = dataSpace;	  			// initialize write pointer
	for (headerCount = 0; headerCount < 16; headerCount++) {	  			// fetch first 16 bytes (== frame header)
		*dataSpaceWritePointer = RPSPI_exchange(0x00);
		maxDataLength--;
		dataSpaceWritePointer++;
	}

	if ((*(dataSpace + 2)) != 0x00) {
		DEBUGOUT2("----- RPSPIFrameRead-Header, warning: status word indicates error: %02X\n",(*(dataSpace + 2)));
		return (-1);
	}

	if ((*(dataSpace + 14)) == 0x02) {			// data header
		unsigned char lsb, msb;
		lsb = *(dataSpace);					//  fetch body length lsb
		msb = *(dataSpace + 1);				//  fetch body lendth msg
		dataCount = (((uint32_t) (msb & 0x0F)) << 8) + ((uint32_t) lsb);
		DEBUGOUT2("RPSPIFrameRead-Header, Data-header states %d bytes of frame data...\n",dataCount);
	} else {			  	 	   				// management header
		if ((*(dataSpace + 14)) == 0x04) {			// data header

			dataCount = (char) (*dataSpace);			// extract length of frame body from header
			if (dataCount == 0xFF) {			//  oups, frame body too int32_t for one byte, we need to look in W4
				unsigned char lsb, msb;
				lsb = *(dataSpace + 8);				//  fetch body length lsb
				msb = *(dataSpace + 9);				//  fetch body lendth msg
				dataCount = (((uint32_t) (msb)) << 8) + ((uint32_t) lsb);
			}
			DEBUGOUT2("RPSPIFrameRead-Header, Management-header states %d bytes of frame data...\n",dataCount);
		} else {
			// spurious signal, ignore!
			return (0);	// we're done, only header required! (-> 16 bytes received)
		}
	}

	bytesRequested = 16 + dataCount;

	if ((dataCount & 0x03) != 0x00) {
		dataCount += 4 - (dataCount & 0x03);
		DEBUGOUT2("DataCount corrected to be multiple of 4, new value: %d\n",dataCount);
	}
	if (dataCount > maxDataLength) {
		DEBUGOUT1("RPSPIFrameRead WARNING: provided memory insufficient, cutting readback!\n");
		dataCount = maxDataLength;
	}

	if (dataCount == 0) {
		DEBUGOUT1("RPSPIFrameRead, no body required -> done\n");
		return (16);	// we're done, only header required! (-> 16 bytes received)
	}
	// now restart a slave read to fetch the data
	cmd &= 0x0000FFFF;						// keep command, clear length
	cmd |= (dataCount & 0xFFFF) << 16;			// length msb,lsb

	DEBUGOUT1("RPSPIFrameRead-Body, re-sending cmd header with new length...\n");
	if (RPSPI_SendCMD(cmd) < 0) {
		return (-1);
	}

	DEBUGOUT1("RPSPIFrameRead-Body, waiting for start token...\n");
	reply = 0;
	timeout = REDPINE_STARTTOKEN_TIMEOUT; // timeout value empirically determined
	while ((reply != 0x55) && (timeout)) {	  	// wait for start token
		timeout--;
		reply = RPSPI_exchange(0x00);
	}
	if (timeout == 0) {
		DEBUGOUT1("RPSPIFrameRead-Body, timeout waiting for start token...\n");
		return (-1);
	}

	// we have received a start token, now fetch header
	DEBUGOUT1("RPSPIFrameRead-Body, retrieving body...\n");
	while (dataCount) {	  			 		// now read all data
		*dataSpaceWritePointer = RPSPI_exchange(0x00);
		dataCount--;
		dataSpaceWritePointer++;
	}

	DEBUGOUT1("RPSPIFrameRead-Body, done\n");
	return (bytesRequested);
}

// *****************************************************************************
int32_t RPSPI_ManagementFrameRead(unsigned char *dataSpace, uint32_t maxDataLength) {
	uint32_t cmd;
	int32_t reply;

	DEBUGOUT1("RPSPIManagementFrameRead:\n");
	cmd = 0x0010045C; // -> read frame  (2: assume "management packets"); data length of frame heade: 0x0010 = 16
	reply = RPSPI_FrameRead(cmd, dataSpace, maxDataLength);
	return (reply);
}

// *****************************************************************************
int32_t RPSPI_DataFrameRead(unsigned char *dataSpace, uint32_t maxDataLength) {
	int32_t cmd, reply;

	DEBUGOUT1("RPSPIDataFrameRead:\n");
	cmd = 0x0010025C; // -> read frame  (2: assume "data packets"); data length of frame header: 0x0010 = 16
	reply = RPSPI_FrameRead(cmd, dataSpace, maxDataLength);
	return (reply);
}
