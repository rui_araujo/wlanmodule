/*
 ===============================================================================
 Name        : main.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
 ===============================================================================
 */

#include "chip.h"
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <cr_section_macros.h>

#include "rp_pinout.h"
#include "rp_spi_driver.h"
#include "utils.h"
#include "RPinterface.h"
#include "RPLib.h"
#include "UART.h"
#include "uart_pinout.h"

#define LED_PORT		0
#define LED_PIN			26

// *************************************** macros
#define LED_ON()		{Chip_GPIO_SetPinOutLow(LPC_GPIO, LED_PORT, LED_PIN);}
#define LED_OFF()		{Chip_GPIO_SetPinOutHigh(LPC_GPIO, LED_PORT, LED_PIN);}
#define LED_TOGGLE()	{Chip_GPIO_SetPinToggle(LPC_GPIO, LED_PORT, LED_PIN);}

// empty to signal end of list

/* Pin muxing configuration */
static const PINMUX_GRP_T pinmuxing[] = {

//UART2
		{ UART2_CTS_PORT, UART2_CTS_PIN, IOCON_MODE_PULLDOWN | IOCON_FUNC0 }, /* CTS2 */
		{ UART2_TX_PORT, UART2_TX_PIN, IOCON_MODE_INACT | IOCON_FUNC1 }, /* TXD2 */
		{ UART2_RX_PORT, UART2_RX_PIN, IOCON_MODE_PULLUP | IOCON_FUNC1 }, /* RXD2 */
		{ UART2_RTS_PORT, UART2_RTS_PIN, IOCON_MODE_INACT | IOCON_FUNC0 }, /* RTS2 */

		//UART0
		{ UART0_TX_PORT, UART0_TX_PIN, IOCON_MODE_INACT | IOCON_FUNC1 }, /* TXD0 */
		{ UART0_RX_PORT, UART0_RX_PIN, IOCON_MODE_PULLUP | IOCON_FUNC1 }, /* RXD0 */
		{ UART0_CTS_PORT, UART0_CTS_PIN, IOCON_MODE_PULLDOWN | IOCON_FUNC0 }, /* CTS0 */
		{ UART0_RTS_PORT, UART0_RTS_PIN, IOCON_MODE_INACT | IOCON_FUNC0 }, /* RTS0 */

		//Redpine Module
		{ RP_SPI_CS_PORT, RP_SPI_CS_PIN, IOCON_MODE_INACT | IOCON_FUNC2 }, /* SPI_CS */
		{ RP_SPI_CLK_PORT, RP_SPI_CLK_PIN, IOCON_MODE_INACT | IOCON_FUNC2 }, /* SPI_CLK */
		{ RP_SPI_MISO_PORT, RP_SPI_MISO_PIN, IOCON_MODE_INACT | IOCON_FUNC2 }, /* SPI_MISO */
		{ RP_SPI_MOSI_PORT, RP_SPI_MOSI_PIN, IOCON_MODE_INACT | IOCON_FUNC2 }, /* SPI_MOSI */
		{ INTERRUPT_PORT, INTERRUPT_PIN, IOCON_MODE_INACT | IOCON_FUNC0 }, /* INTR */
		{ WLAN_RESET_PORT, WLAN_RESET_PIN, IOCON_MODE_INACT | IOCON_FUNC0 }, /* RESET_WLAN */

		{ LED_PORT, LED_PIN, IOCON_MODE_PULLDOWN | IOCON_FUNC0 }, /* Led 0 */

};

long ledState;	 			// 0:off, -1:on, -2:blinking, >0: timeOn

const int8_t NetworkNameKeyType[][32] = {		 		// key: 0-none, 1-wpa, 2-wpa2, 3-wep
//	 "NSTrobots", "rpo45trkrfgpoloektr45poael", "2",		// NST Network @ Karlstr
#ifdef USE_22_MODULE					// 2.4 GHz		"NSTrobots_2.4GHz", "rpo45trkrfgpoloektr45poael", "2",		// NST Network @ Karlstr#endif#ifdef USE_28_MODULE					// 5 GHz				"NSTrobots_5GHz", "rpo45trkrfgpoloektr45poael", "2",		// NST Network @ Karlstr#endif//	 "WLAN-INI", "04BE71EE77", "3",							// INI Zurich//	 "TrivaElefant", "0585009799251538", "2",				// Home//	 "JCMobile", "2512jc74", "2",			   	 			// mobile phone Backup (emergency solution)//	 "AndreouLab", "HelloUrania", "1",			   	 		// Andreas Andreou Network//	 "AGALab2Go", "HelloOlympus", "2",			   	 		// Andreas Andreou Network
				"" };		 			 								// empty to signal end of list

long socketHandleTCP0, socketHandleUDP0, socketHandleTCPRP0;
uint32_t ipAddressMemory0;
uint32_t uart0ActiveSocket;

uint32_t socketHandleTCP1, socketHandleUDP1, socketHandleTCPRP1;
uint32_t ipAddressMemory1;
uint32_t uart1ActiveSocket;

uint32_t myCurrentIPAddress;
uint32_t myMACAddress[6];

extern uint32_t myCurrentIPAddress;

uint32_t socketHandleCTRLTCP, socketHandleCTRLUDP;
uint32_t ipAddressMemoryCTRL;

extern uint32_t UARTCommandModeActive;
uint32_t commandModeEchoActive = 1;

extern uint32_t lastUART0TimeOutCharacter;   		   // if timeout, remember last character (for reprogramming mode)

// *****************************************************************************
void delayMS(uint32_t delayTimeMS) {
	uint32_t m, n;
#define DELAY_1MS	  ((unsigned short) ((0x5A)*64))
	for (n = 0; n < delayTimeMS; n++) {
		for (m = DELAY_1MS; m; m--) {
		}
	}
}

// *****************************************************************************
void LEDSetState(long state) {
	ledState = state;
	if (state != 0) {
		LED_ON()
		;
	} else {
		LED_OFF()
		;
	}
}


// *****************************************************************************
void signalLED(long count) {

	//extern uint8_t TCPqueue0[];
	//extern uint32_t TCPpointer0;

	while (count) {
		LED_ON()
		;
		delayMS(200);

		LED_OFF()
		;
		delayMS(200);
		count--;

		if (lastUART0TimeOutCharacter == 64) {		   					// "@" transmitted?
			__disable_irq();
			LPC_UART0->THR = '-';
			LPC_UART0->THR = 'r';
			LPC_UART0->THR = 'p';
			LPC_UART0->THR = 'M';
			LPC_UART0->THR = 'o';
			LPC_UART0->THR = 'd';
			LPC_UART0->THR = 'e';
			LPC_UART0->THR = '-';
			LPC_UART0->THR = 10;
			LPC_UART0->THR = 13;
			while ((LPC_UART0->LSR & _BIT(6)) == 0) {
			};		   // wait for UART to finish data transfer
			enterReprogrammingMode();
		}
	}
}

// *****************************************************************************
void autoOpenSockets(void) {
	if (socketHandleTCP0 == 0) {
#ifdef AUTO_CONNECT_TCP
#ifdef AUTO_CONNECT_SOCKET_OFFSET
		DEBUGOUT6("\n\n********** opening socket for TCP0, Port %d to IP %d.%d.%d.%d\n", BASE_PORT+TCP_PORT_OFFSET,
		((myCurrentIPAddress&0xFF000000)>>24), ((myCurrentIPAddress&0xFF0000)>>16),
		((myCurrentIPAddress&0xFF00)>>8), ((myCurrentIPAddress&0xFF)+AUTO_CONNECT_SOCKET_OFFSET));
		socketHandleTCP0 = RP_DF_OPEN_TCP_CLIENT(BASE_PORT+TCP_PORT_OFFSET, BASE_PORT+TCP_PORT_OFFSET, myCurrentIPAddress+AUTO_CONNECT_SOCKET_OFFSET);
#endif
#ifdef AUTO_CONNECT_FIXED_IP
		DEBUGOUT6("\n\n********** opening socket for TCP0, Port %d to IP %d.%d.%d.%d\n", BASE_PORT+TCP_PORT_OFFSET,
		((AUTO_CONNECT_FIXED_IP&0xFF000000)>>24), ((AUTO_CONNECT_FIXED_IP&0xFF0000)>>16),
		((AUTO_CONNECT_FIXED_IP&0xFF00)>>8), ((AUTO_CONNECT_FIXED_IP&0xFF)));
		socketHandleTCP0 = RP_DF_OPEN_TCP_CLIENT(BASE_PORT+TCP_PORT_OFFSET, BASE_PORT+TCP_PORT_OFFSET, AUTO_CONNECT_FIXED_IP);
#endif

		if (socketHandleTCP0 < 0) {
			UARTCommandModeActive = 1;  	   // go into UART command mode to find out what's going on!
		}

		uart0ActiveSocket = socketHandleTCP0;
#else
		DEBUGOUT2("\n\n********** opening socket for TCP0, Port %d\n", BASE_PORT + TCP_PORT_OFFSET);
		socketHandleTCP0 = RP_DF_OPEN_TCP_LISTENER(BASE_PORT + TCP_PORT_OFFSET);
#endif
		DEBUGOUT2("TCP0 handle %d\n", socketHandleTCP0);
		uart0WaitForTransferDone();
	}
	if (socketHandleUDP0 == 0) {
#ifdef AUTO_CONNECT_UDP
		long otherUDPPort;
#ifdef AUTO_CONNECT_SOCKET_OFFSET
		otherUDPPort = BASE_PORT+(((myCurrentIPAddress+AUTO_CONNECT_SOCKET_OFFSET)&0xFF));
		DEBUGOUT6("\n\n********** opening socket for UDP0, Port %d to IP %d.%d.%d.%d\n", otherUDPPort,
		((myCurrentIPAddress&0xFF000000)>>24), ((myCurrentIPAddress&0xFF0000)>>16),
		((myCurrentIPAddress&0xFF00)>>8), ((myCurrentIPAddress&0xFF)+AUTO_CONNECT_SOCKET_OFFSET));

		socketHandleUDP0 = RP_DF_OPEN_UDP_CLIENT(otherUDPPort, otherUDPPort, myCurrentIPAddress+AUTO_CONNECT_SOCKET_OFFSET);
		ipAddressMemory0 = myCurrentIPAddress+AUTO_CONNECT_SOCKET_OFFSET;
#endif
#ifdef AUTO_CONNECT_FIXED_IP
		otherUDPPort = BASE_PORT+(((AUTO_CONNECT_FIXED_IP)&0xFF));
		DEBUGOUT6("\n\n********** opening socket for UDP0, Port %d to IP %d.%d.%d.%d\n", otherUDPPort,
		((AUTO_CONNECT_FIXED_IP&0xFF000000)>>24), ((AUTO_CONNECT_FIXED_IP&0xFF0000)>>16),
		((AUTO_CONNECT_FIXED_IP&0xFF00)>>8), ((AUTO_CONNECT_FIXED_IP&0xFF)));

		socketHandleUDP0 = RP_DF_OPEN_UDP_CLIENT(otherUDPPort, otherUDPPort, AUTO_CONNECT_FIXED_IP);
		ipAddressMemory0 = AUTO_CONNECT_FIXED_IP;
#endif

		if (socketHandleUDP0 < 0) {
			UARTCommandModeActive = 1;  	   // go into UART command mode to find out what's going on!
		}

		uart0ActiveSocket = socketHandleUDP0;
#else
		DEBUGOUT2("\n\n********** opening socket for UDP0, Port %d\n", BASE_PORT + UDP_PORT_OFFSET);
		socketHandleUDP0 = RP_DF_OPEN_UDP_LISTENER(BASE_PORT + UDP_PORT_OFFSET);
#endif
		DEBUGOUT2("UDP0 handle %d\n", socketHandleUDP0);
		uart0WaitForTransferDone();
	}
	if (socketHandleTCPRP0 == 0) {
		DEBUGOUT2("\n\n********** opening socket for RP-TCP0, Port %d\n", BASE_PORT + TCP_REPROG_PORT_OFFSET);
		socketHandleTCPRP0 = RP_DF_OPEN_TCP_LISTENER(BASE_PORT + TCP_REPROG_PORT_OFFSET);
		DEBUGOUT2("TCPRP0 handle %d\n", socketHandleTCPRP0);
		uart0WaitForTransferDone();
	}

	if (socketHandleTCP1 == 0) {
#ifdef AUTO_CONNECT_TCP
#ifdef AUTO_CONNECT_SOCKET_OFFSET
		DEBUGOUT6("\n\n********** opening socket for TCP1, Port %d to IP %d.%d.%d.%d\n", BASE_PORT+UART1_PORT_OFFSET+TCP_PORT_OFFSET,
		((myCurrentIPAddress&0xFF000000)>>24), ((myCurrentIPAddress&0xFF0000)>>16),
		((myCurrentIPAddress&0xFF00)>>8), ((myCurrentIPAddress&0xFF)+AUTO_CONNECT_SOCKET_OFFSET));
		socketHandleTCP1 = RP_DF_OPEN_TCP_CLIENT(BASE_PORT+UART1_PORT_OFFSET+TCP_PORT_OFFSET, BASE_PORT+UART1_PORT_OFFSET+TCP_PORT_OFFSET, myCurrentIPAddress+AUTO_CONNECT_SOCKET_OFFSET);
#endif
#ifdef AUTO_CONNECT_FIXED_IP
		DEBUGOUT6("\n\n********** opening socket for TCP1, Port %d to IP %d.%d.%d.%d\n", BASE_PORT+UART1_PORT_OFFSET+TCP_PORT_OFFSET,
		((AUTO_CONNECT_FIXED_IP&0xFF000000)>>24), ((AUTO_CONNECT_FIXED_IP&0xFF0000)>>16),
		((AUTO_CONNECT_FIXED_IP&0xFF00)>>8), ((AUTO_CONNECT_FIXED_IP&0xFF)));
		socketHandleTCP1 = RP_DF_OPEN_TCP_CLIENT(BASE_PORT+UART1_PORT_OFFSET+TCP_PORT_OFFSET, BASE_PORT+UART1_PORT_OFFSET+TCP_PORT_OFFSET, AUTO_CONNECT_FIXED_IP);
#endif

		if (socketHandleTCP1 < 0) {
			UARTCommandModeActive = 1;  	   // go into UART command mode to find out what's going on!
		}

		uart1ActiveSocket = socketHandleTCP1;
#else
		DEBUGOUT2("\n\n********** opening socket for TCP1, Port %d\n", BASE_PORT+UART1_PORT_OFFSET+TCP_PORT_OFFSET);
		socketHandleTCP1 = RP_DF_OPEN_TCP_LISTENER(BASE_PORT+UART1_PORT_OFFSET+TCP_PORT_OFFSET);
#endif
		DEBUGOUT2("TCP1 handle %d\n", socketHandleTCP1);
		uart0WaitForTransferDone();
	}
	if (socketHandleUDP1 == 0) {
#ifdef AUTO_CONNECT_UDP
		long otherUDPPort;
#ifdef AUTO_CONNECT_SOCKET_OFFSET
		otherUDPPort = BASE_PORT+UART1_PORT_OFFSET+(((myCurrentIPAddress+AUTO_CONNECT_SOCKET_OFFSET)&0xFF));
		DEBUGOUT6("\n\n********** opening socket for UDP1, Port %d to IP %d.%d.%d.%d\n", otherUDPPort,
		((myCurrentIPAddress&0xFF000000)>>24), ((myCurrentIPAddress&0xFF0000)>>16),
		((myCurrentIPAddress&0xFF00)>>8), ((myCurrentIPAddress&0xFF)+AUTO_CONNECT_SOCKET_OFFSET));

		socketHandleUDP1 = RP_DF_OPEN_UDP_CLIENT(otherUDPPort, otherUDPPort, myCurrentIPAddress+AUTO_CONNECT_SOCKET_OFFSET);
		ipAddressMemory1 = myCurrentIPAddress+AUTO_CONNECT_SOCKET_OFFSET;
#endif
#ifdef AUTO_CONNECT_FIXED_IP
		otherUDPPort = BASE_PORT+UART1_PORT_OFFSET+(((AUTO_CONNECT_FIXED_IP)&0xFF));
		DEBUGOUT6("\n\n********** opening socket for UDP1, Port %d to IP %d.%d.%d.%d\n", otherUDPPort,
		((AUTO_CONNECT_FIXED_IP&0xFF000000)>>24), ((AUTO_CONNECT_FIXED_IP&0xFF0000)>>16),
		((AUTO_CONNECT_FIXED_IP&0xFF00)>>8), ((AUTO_CONNECT_FIXED_IP&0xFF)));

		socketHandleUDP1 = RP_DF_OPEN_UDP_CLIENT(otherUDPPort, otherUDPPort, AUTO_CONNECT_FIXED_IP);
		ipAddressMemory1 = AUTO_CONNECT_FIXED_IP;
#endif

		if (socketHandleUDP1 < 0) {
			UARTCommandModeActive = 1;  	   // go into UART command mode to find out what's going on!
		}

		uart1ActiveSocket = socketHandleUDP1;
#else
		DEBUGOUT2("\n\n********** opening socket for UDP1, Port %d\n", BASE_PORT+UART1_PORT_OFFSET+UDP_PORT_OFFSET);
		socketHandleUDP1 = RP_DF_OPEN_UDP_LISTENER(BASE_PORT+UART1_PORT_OFFSET+UDP_PORT_OFFSET);
#endif
		DEBUGOUT2("UDP1 handle %d\n", socketHandleUDP1);
		uart0WaitForTransferDone();
	}
	if (socketHandleTCPRP1 == 0) {
		DEBUGOUT2("\n\n********** opening socket for RP-TCP1, Port %d\n", BASE_PORT+UART1_PORT_OFFSET+TCP_REPROG_PORT_OFFSET);
		socketHandleTCPRP1 = RP_DF_OPEN_TCP_LISTENER(BASE_PORT+UART1_PORT_OFFSET+TCP_REPROG_PORT_OFFSET);
		DEBUGOUT2("TCPRP1 handle %d\n", socketHandleTCPRP1);
		uart0WaitForTransferDone();
	}

#ifdef ENABLE_TCP_DEBUG_PORT
	if (socketHandleCTRLTCP == 0) {
		DEBUGOUT2("\n\n********** opening socket for CTRL-TCP, Port %d\n", BASE_PORT+CTRL_PORT_TCP_OFFSET);
		socketHandleCTRLTCP = RP_DF_OPEN_TCP_LISTENER(BASE_PORT+CTRL_PORT_TCP_OFFSET);
		DEBUGOUT2("CTRL-TCP handle %d\n", socketHandleCTRLTCP);
		uart0WaitForTransferDone();
	}
#endif
#ifdef ENABLE_UDP_DEBUG_PORT
	if (socketHandleCTRLUDP == 0) {
		DEBUGOUT2("\n\n********** opening socket for CTRL-UDP, Port %d\n", BASE_PORT+CTRL_PORT_UDP_OFFSET);
		socketHandleCTRLUDP = RP_DF_OPEN_UDP_LISTENER(BASE_PORT+CTRL_PORT_UDP_OFFSET);
		DEBUGOUT2("CTRL-UDP handle %d\n", socketHandleCTRLUDP);
		uart0WaitForTransferDone();
	}
#endif

}

// *****************************************************************************
void powerUpAttachToNetwork(void) {
	long n;						   // try several times to connect, give up eventually

	for (n = 0; n < 3; n++) {			   // try connecting three times

		signalLED(1);
		RP_HardwareReset();
		signalLED(2);

		if (RP_BootModule() >= 0) {
			signalLED(3);

			if (RP_InitModule() >= 0) {
				signalLED(4);	  	 	   // success!
				return;
			}
		}

	}  // end of multiple trials
	UARTCommandModeActive = 1;  // go into UART command mode to find out what's going on!
}

// *****************************************************************************
// ************************************************************* Mainloop
// *****************************************************************************
void mainloop(void) {
	MLStart:

// *****************************************************************************
//    LEDIterate();
// *****************************************************************************
	if (ledState) {
		if (ledState > 0) {
			ledState--;
			if (ledState == 1) {
				ledState = 0;
				LED_OFF()
				;
			}
		} else {
			if (ledState < 0) {
				ledState++;
				if (ledState == 0) {
					LED_TOGGLE()
					;
					if (UARTCommandModeActive) {
						ledState = ((long) -10000);
					} else {

						if ((uart0ActiveSocket != 0) || (uart1ActiveSocket != 0)) {
							ledState = ((long) -25000);  	// indicate that one socket is active
						} else {
							ledState = ((long) -50000);
						}
					}
				}
			}
		}
	}

// *****************************************************************************
//    UARTIterate();
// *****************************************************************************
	uartIterate();

// *****************************************************************************
//    Re-Open Disconnected Sockets
// *****************************************************************************
	if (myCurrentIPAddress) {
		autoOpenSockets();   	  		  		 // reopen possibly disconnected sockets
	}

// *****************************************************************************
//    WLAN Iterate();
// *****************************************************************************
	if (Chip_GPIO_ReadPortBit(LPC_GPIO, INTERRUPT_PORT, INTERRUPT_PIN)) {
		RP_InterruptDataAvailable();
	}

// *****************************************************************************
	goto MLStart;

}

int main(void) {

// Read clock settings and update SystemCoreClock variable
	SystemCoreClockUpdate();
	Chip_GPIO_Init(LPC_GPIO);
	Chip_IOCON_Init(LPC_IOCON);
	Chip_RIT_Init(LPC_RITIMER);
	Chip_RIT_Enable(LPC_RITIMER);
	Chip_IOCON_SetPinMuxing(LPC_IOCON, pinmuxing, sizeof(pinmuxing) / sizeof(PINMUX_GRP_T));
	Chip_Clock_SetPCLKDiv(SYSCTL_PCLK_SSP1, SYSCTL_CLKDIV_1);
	Chip_Clock_SetPCLKDiv(SYSCTL_PCLK_UART0, SYSCTL_CLKDIV_1);
	Chip_Clock_SetPCLKDiv(SYSCTL_PCLK_UART2, SYSCTL_CLKDIV_1);
	Chip_GPIO_SetPinOutHigh(LPC_GPIO, LED_PORT, LED_PIN);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, LED_PORT, LED_PIN);
	LEDSetBlinking();

	socketHandleTCP0 = 0;
	socketHandleUDP0 = 0;
	socketHandleTCPRP0 = 0;
	uart0ActiveSocket = 0;

	socketHandleTCP1 = 0;
	socketHandleUDP1 = 0;
	socketHandleTCPRP1 = 0;
	uart1ActiveSocket = 0;

	socketHandleCTRLTCP = 0;
	socketHandleCTRLUDP = 0;

	uartInit();

	RP_Init();

#ifdef INCLUDE_RP_FIRMWARE_UPGRADE
	UARTCommandModeActive=1;
#else
	powerUpAttachToNetwork();
#endif

	// *****************************************************************************
	//    Start Main Loop
	// *****************************************************************************
	mainloop();

	// will never return here
#if 0
	UARTInit(LPC_UART0, 4000000);
	UARTInit(LPC_UART2, 4000000);
	SPI_Init();
// Force the counter to be placed into memory
	volatile static uint32_t i = LOOP;
// Enter an infinite loop, just incrementing a counter
	while (1) {
		if ( LPC_UART0->LSR & UART_LSR_RDR) {
			LPC_UART0->THR = LPC_UART0->RBR;
		}
		if ( LPC_UART2->LSR & UART_LSR_RDR) {
			LPC_UART2->THR = LPC_UART2->RBR;
		}
		i--;
		if (i == 0) {
			i = LOOP;
			Chip_GPIO_SetPinToggle(LPC_GPIO, LED_PORT, LED_PIN);
		}
	}
	return 0;
#endif
}
